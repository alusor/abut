# Abut
A React Native App.
- [Enviroment config.](#Enviroment-config)
- [How to run the project.](#How-to-run-the-project)
- [Stack.](#Stack)
- [Project structure.](#Project-structure)
- [How-to](#How-to)
- [Resources](#Resources)
## Enviroment config.
Requirements:
```
- Xcode 10
- Android Studio 3.2
- Node 8 or newer
- React Native CLI (requires node)
- Optional: i use yarn instead of npm for package managment
```
To install follow the instructions [here 'Building Projects with Native Code'](https://facebook.github.io/react-native/docs/getting-started.html)

Note: for android be sure to install the versions of sdk and build tools listed in the instructions.
## How to run the project.
1. Clone the project and install the dependencies. 
    ```console
    $ git clone https://alusor@bitbucket.org/alusor/abut.git
    $ cd abut && yarn
    ```
2. To run on android:
*  Open android emulator or conect a device, then open the terminal in the project directory and run: 
    ```console
    $ react-native run-android
    ```
*    Other way to run the project is opening with android studio and running from them, but be sure that the dependencies are correctly installed.

## Stack.
React Native as a framework is good but for certain cases and since react is only for the UI we need to use other modules/libraries to handle some use cases.
The modules are listed here:

- **React Navigation:** Handle the navigation of the app
- **Redux:** State managment.
- **React Redux:** To connect the react components to the global state.
- **Redux Saga:** To handle side effects, for example: handle a request to server outside a react component and then injecting the result to the store
- **React Native Maps:** Components of the google maps sdk for react native
- **React Native Orientation Locker:** Control the orientation of the device. 
- **React Native fbsdk:** Facebook native sdk for login
- **React Native Google Signin:** Google native sdk for login
- **React Native Vector Icons:** To use some webfonts with icons.
- **Styled Components**: To create React Components with styles with a sintax like css 

## Project structure.
```
./
| android/ [Android native project]
| ios/ [ios native project]
| node_modules/ [node modules used for whole the project]
| src/ [all javascript of the project, our source code]
|    actions/ [redux actions, and action creators]
|    components/ [common components used more than one time in the app]
|    icons/ [Images extrated from the adobe XD file]
|    navigations/ [Navigation confing and helpers to handle navigation outside react componets]
|    reducers/ [redux reducers]
|    sagas/ [Redux sagas]
|    services/ [Here are a file with all the requests to a backend]
|    store/ [Global store config]
|    views/ [here are all views of the app]
| App.js [Root react component]
| index.js [File tro register/link our react app to the native projects]
| ios_setup.js [File to confing the facebook sdk for ios]
| package.json [File that list our node dependencies]

```
## How-to
1. ### Create a new app view.

    >   Create a new file in views folder
        Create a react component for the view it could be a class component or stateless.
        
    **views/Example.js**   
    ```js
    import React, { Component } from 'react';
    import { View } from 'react-native';
    
    class Example extends Component {
        render() {
            return(
            <View/>
            );
        } 
    }
    export default Example;
    ```
    > Now you need to register in the mainNavigation.js file inside navigations directory
    
    **navigations/mainNavigation.js**
    ```js
    imports ........
    import '../views/Example.js';
    ```
    > Then you need to register in one of the navigation containers, for example in this file for the Home Grid i have a stack navigator, so if i need the new page inside them only need to add it.
    
    ```js
    const Grid = createStackNavigator({
      Home,
      Detail,
      PresaleDetail,
      Example
    }, {
      navigationOptions: {
        header: null
      }
    });
    ```
    > Now if you want to navigate to Example, you only need to call the navigation method with the name of the view, in the view that will be navigating to them, for example:
    ```js
    handleNavigation = () => this.props.navigation.navigate('Example');
    ```
2. ### Connecting component to the store
    > To connect components to the store only need to use the HOC provided by react-redux (connect)
    
    **Example.js**
    ```js
    import React, { Component } from 'react';
    import { View } from 'react-native';
    import { connect } from 'react-redux';
    
    class Example extends Component {
        render() {
            return(
            <View/>
            );
        } 
    }
    export default connect()(Example);
    ```
3. ### Creating actions
    > Every action in the app has a definition(type) and a creator, every file can contain multiple actions.
    > The actions need to be related to the data that they will be injecting.
    ```js
    //Type
    const REQUEST_NEW_USER = 'REQUEST_NEW_USER';
    
    //Creator
    const requestNewUser = (data) => ({
        //Type of the creator
        type: REQUEST_NEW_USER,
        //[optional] if the creator need to set data
        payload: data
    })
    
    //then to export them
    module.export = {
        types: {
            REQUEST_NEW_USER
        },
        creators: {
            requestNewUser
        }
    }
    ```
4. ### Creating reducers
    > Reducers specify how the application's state changes in response to actions sent to the store. Remember that actions only describe what happened, but don't describe how the application's state changes.
    
    **reducers/user.js**
    ```js
    //Import types to use in this reducer
    import {types} from '../actions/User';
    
    //It's a good idea to have an initial state for the reducer whit a basic structure
    const initialState = {
      user: null,
      selectedUserId: null,
      currentUserId: null,
      selectedUser: {},
      isLoading: false,
      newUser: null
    };

    //Finally the reducer
    const userReducer = (state = initialState, action) => {
      //This is because every creator need to have an action type
      switch (action.type) {
        //For example here i add the action created in the previus step
        case types.REQUEST_NEW_USER:
          //Then if the type match we need to return a new state.
          return { ...state, newUser: action.payload }
        default:
          return state;
      }
    };
    ```
    >Now you need to add the reducer to the reducers/index.js file to create a combined reducer
    
    **reducers/index.js**
    ```js
    //Import function that combine our reducers in a single reducer
    import {combineReducers} from 'redux';
    
    //Import our reducers
    import userReducer from './User';
    import propertyReducer from './Properties';
    
    //Create a new reducer
    const rootReducer = combineReducers({
      userReducer,
      propertyReducer
    });
    
    //Export the created reducer
    export default rootReducer;
    ```
5. ### Handling actions with sagas
    >redux-saga is a library that aims to make application side effects (i.e. asynchronous things like data fetching and impure things like accessing the browser cache) easier to manage, more efficient to execute, simple to test, and better at handling failures.
    
    [Documentation](https://redux-saga.js.org/docs/basics/UsingSagaHelpers.html)
    >Now you have an action and a reducer, but our action REQUEST_NEW_USER require to fetch some data to the server, to this we need to create other file that can hear when an action occurs
    
    **sagas/User.js**
    ```js
    //Import effects that you will be using
    import {takeLatest, select, put, call} from 'redux-saga/effects';
    
    //import types and creators
    import {types as userTypes, creators as userCreators} from '../actions/User';
    
    //Import a service to use
    import { newUser } from '../services';
    
    //Function that return new user from global store
    const getNewUserData = ({ userReducer }) => userReducer.newUser;
    
    //Our function that will be executed when an action is triggered
    function* createUser(){
    
      //the select effect will run the function that return the new user data.
      const newUserData = yield select(getNewUserData);
      
      //Prepare the object that will be send to the backend       
      const newUserO = JSON.stringify({
        name: newUserData.nombre,
        lastName: newUserData.apellido,
        password: newUserData.password,
        email: newUserData.email,
        Provider: null,
        ID_Provider: null
      });
      
     try{
     // the call effect will call our request with the object created
      const request = yield call(newUser, newUserO);
      
      //if the request was successful the put effect will inject a new action with the data returned from the backend
      yield put(userCreators.requestNewUserComplete(request[0]));
     } catch(e) {
        //If the request failed we need to inject an failure action
       yield put(userCreators.requestNewUserFailed(true));
     };
    }


    export function* userSaga() {
      //Here our saga will be listeting for every REQUEST_NEW_USER and after executing the createUser function
      yield takeLatest(userTypes.REQUEST_NEW_USER, createUser);
    }
    ```
6. ### Creating a service(request)
    >Creating a request its so simple, we only need to define them and export it, they have a basic structure.
    
    **services/index.js**
    ```js
    //URL for every request
    const BASE_URL = 'http://api.myabut.com/public'
    
    //Every function need to be async
    // function that return a list of properties
    async function getProperties() {
    
      //Need to do all request with the fetch standard
      const response = await fetch(`${BASE_URL}/GetProperties`);
      
      //Afer the request occurs you need to get a json.
      const data = await response.json();
      
      //Finally return the object. 
      return data.properties;
    }
    
    //Export it 
    module.exports = {
        getProperties
    }

    ```
## Resources
Resources to learn or understand how to work with the main libraries/modules.
- [React Native Docs](https://facebook.github.io/react-native/docs/)
- [react-native-maps](https://github.com/react-native-community/react-native-maps)
- [Redux docs in spanish](https://es.redux.js.org/)
- [Redux-saga](https://redux-saga.js.org/)
- [styled-components](https://www.styled-components.com/)
- [react-navigation v2](https://reactnavigation.org/docs/en/2.x/getting-started.html)
