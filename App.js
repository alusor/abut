import App from './src/navigations/mainNavigation';
import React from 'react';
import { Provider } from 'react-redux';
import Store from './src/store/store';
import NavigationService from './src/navigations/NavigationService';
import Orientation from 'react-native-orientation-locker';



export default class extends React.Component {
    componentWillMount() {
        Orientation.lockToPortrait();
    }
    componentDidMount() {
        Orientation.addOrientationListener(this._onOrientationDidChange);
    }
    _onOrientationDidChange = (orientation) => {
        Orientation.getDeviceOrientation(orientation => console.log(orientation));
        Orientation.getOrientation((orientation,deviceOrientation)=> {
            console.log("Current UI Orientation: ", orientation);
            console.log("Current Device Orientation: ", deviceOrientation);
          });
    }
    render() {
        return (
            <Provider store={Store}>
                <App  ref={navigatorRef => {
                  NavigationService.setTopLevelNavigator(navigatorRef);
                }}/>
            </Provider>
        );
    }
}