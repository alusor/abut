import React from 'react';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';
import {Image} from 'react-native';
import Home from '../views/Home';
import PropertyPublish from '../views/Properties/PropertyPublish';
import PropertyDetails from '../views/Properties/PropertyDetails';
import PropertyDataSheet from '../views/Properties/PropertyDataSheet';
import Muro from '../views/Muro/Muro';
import Buscar from '../views/Buscar';
import Perfil from '../views/Perfil';
import Password from '../views/Session/Password';
import Splash from '../views/Session/Splash';
import Login from '../views/Session/Login';
import Detail from '../views/Muro/Detail';
import Calendar from '../views/Calendar';
import OtherProfile from '../views/OtherProfile';
import PresaleDetail from '../views/Muro/PresaleDetail';
//Maps views
import Filter from '../views/Filter';
import BuscarLista from '../views/Buscar_Lista';
import SearchFilter from '../views/Search/SearchFilter';
//Control
import MyProperties from '../views/control/MyProperties';
import Blank from '../views/control/Blank';

const inicio = require('../icons/home.png');
const muro = require('../icons/muro.png');
const publicar = require('../icons/publicar.png');
const buscar = require('../icons/buscar.png');
const calendario = require('../icons/calendario.png');

const Calendario = createStackNavigator({
  Calendar
}, {
  navigationOptions: {
    header: null
  }
});
const Mapa = createStackNavigator({
  Buscar,
  DetailMap: {screen: Detail},
}, {
  navigationOptions: {
    header: null
  }
});
const MuroOportunidades = createStackNavigator({
  Muro,
  DetailP: {screen: Detail},
  Filter,

}, {
  navigationOptions: {
    header: null
  }
});
const MainNavigation = createStackNavigator({
  PropertyPublish,
  PropertyDetails,
  PropertyDataSheet
}, {
  navigationOptions: {
    header: null
  }
});
const style = {
  iconstyle: {
    width: 25,
    height: 25,
    resizeMode: 'contain'
  }
}
const BuscarStack = createStackNavigator({
  Mapa,
  BuscarLista,
  SearchFilter,
}, {
  navigationOptions: {
    header: null
  }
});
const Grid = createStackNavigator({
  Home,
  Detail,
  PresaleDetail
}, {
  navigationOptions: {
    header: null
  }
});

const Tabs = createBottomTabNavigator({
  Inicio: {
    screen: Grid, navigationOptions: {
      tabBarIcon: ({tintColor}) => <Image style={[style.iconstyle, {tintColor}]} source={inicio}/>
    }
  },
  Muro: {
    screen: MuroOportunidades, navigationOptions: {
      tabBarIcon: ({tintColor}) => <Image style={[style.iconstyle, {tintColor}]} source={muro}/>
    }
  },
  Publicar: {
    screen: MainNavigation, navigationOptions: {
      tabBarIcon: <Image style={style.iconstyle} source={publicar}/>

    }
  },
  Buscar: {
    screen: BuscarStack, navigationOptions: {
      tabBarIcon: ({tintColor}) => <Image style={[style.iconstyle, {tintColor}]} source={buscar}/>
    }
  },
  Calendario: {
    screen: Calendario, navigationOptions: {
      tabBarIcon: ({tintColor}) => <Image style={[style.iconstyle, {tintColor}]} source={calendario}/>
    }
  },

}, {
  tabBarOptions: {
    activeTintColor: '#56ADE3'
  }
});

const Session = createStackNavigator({
  Login,
  Splash,
  Tabs,
  Password,
  MyProperties,
  Blank,
  Perfil,
  OtherProfile
}, {
  navigationOptions: {
    header: null
  }
});

export default Session;


