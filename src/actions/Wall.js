const REQUEST_OPORTNITY_WALL = 'REQUEST_OPORTUNITY_WALL';
const REQUEST_OPORTNITY_WALL_COMPLETE = 'REQUEST_OPORTNITY_WALL_COMPLETE';
const REQUEST_OPORTNITY_WALL_FAILED = 'REQUEST_OPORTUNITY_WALL_FAILED';

const requestOportunityWall = () => ({
  type: REQUEST_OPORTNITY_WALL
});

const requestOportunityWallComplete = (posts) => ({
  type: REQUEST_OPORTNITY_WALL_COMPLETE,
  payload: posts
});

const requestOportunityWallFailed = (error) => ({
  type: REQUEST_OPORTNITY_WALL_FAILED,
  payload: error
});

module.exports = {
  types: {
    REQUEST_OPORTNITY_WALL,
    REQUEST_OPORTNITY_WALL_COMPLETE,
    REQUEST_OPORTNITY_WALL_FAILED,
  },
  creators: {
    requestOportunityWall,
    requestOportunityWallComplete,
    requestOportunityWallFailed
  }
}