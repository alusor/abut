const REQUEST_EMAIL_LOGIN = 'REQUEST_EMAIL_LOGIN';
const REQUEST_SOCIAL_LOGIN = 'REQUEST_SOCIAL_LOGIN';
const LOGIN_RESPONSE = 'LOGIN_RESPONSE';
const LOGIN_COMPLETE = 'LOGIN_COMPLETE';
const LOGIN_FAILED = 'LOGIN_FAILED';
const REQUEST_NEW_USER = 'REQUEST_NEW_USER';
const REQUEST_NEW_USER_COMPLETE = 'REQUEST_NEW_USER_COMPLETE';
const REQUEST_NEW_USER_FAILED = 'REQUEST_NEW_USER_FAILED';

const requestEmailLogin = (login) => ({
  type: REQUEST_EMAIL_LOGIN,
  payload: login
});
const requestSocialLogin = () => ({
  type: REQUEST_SOCIAL_LOGIN
});

const loginResponse = (response) => ({
  type: LOGIN_RESPONSE,
  payload: response
});

const loginComplete = (user) => ({
  type: LOGIN_COMPLETE,
  payload: user
});
const loginFailed = (error) => ({
  type: LOGIN_FAILED,
  payload: error,
});

const requestNewUser = (user) => ({
  type: REQUEST_NEW_USER,
  payload: user,
});
const requestNewUserComplete = (user) => ({
  type: REQUEST_NEW_USER_COMPLETE,
  payload: user
});

const requestNewUserFailed = (error) => ({
  type: REQUEST_NEW_USER_FAILED,
  payload: error
});

//User general profile
const REQUEST_USER_PROFILE = 'REQUEST_USER_PROFILE';
const REQUEST_USER_PROFILE_COMPLETE = 'REQUEST_USER_PROFILE_COMPLETE';
const REQUEST_USER_PROFILE_FAILED = 'REQUEST_USER_PROFILE_FAILED';

const requestUserProfile = (id) => ({
  type: REQUEST_USER_PROFILE,
  payload: id
});
const requestUserProfileComplete = (user) => ({
  type: REQUEST_USER_PROFILE_COMPLETE,
  payload: user
});
const requestUserProfileFailed = (error) => ({
  type: REQUEST_USER_PROFILE_FAILED,
  payload: error,
});


module.exports = {
  types: {
    REQUEST_SOCIAL_LOGIN,
    REQUEST_EMAIL_LOGIN,
    LOGIN_RESPONSE,
    LOGIN_COMPLETE,
    LOGIN_FAILED,
    REQUEST_USER_PROFILE,
    REQUEST_USER_PROFILE_COMPLETE,
    REQUEST_USER_PROFILE_FAILED,
    REQUEST_NEW_USER,
    REQUEST_NEW_USER_COMPLETE,
    REQUEST_NEW_USER_FAILED
  },
  creators: {
    requestEmailLogin,
    requestSocialLogin,
    loginResponse,
    loginComplete,
    loginFailed,
    requestUserProfile,
    requestUserProfileComplete,
    requestUserProfileFailed,
    requestNewUser,
    requestNewUserFailed,
    requestNewUserComplete
  }
};