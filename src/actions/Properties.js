const REQUEST_PROPERTIES = 'REQUEST_PROPERTIES';
const REQUEST_PROPERTIES_COMPLETE = 'REQUEST_PROPERTIES_COMPLETE';
const REQUEST_PROPERTIES_FAILED = 'REQUEST_PROPERTIES_FAILED';
const PUBLISH_PROPERTY_ONE = 'PUBLISH_PROPERTY_ONE';
const PUBLISH_PROPERTY_PREVIEW = 'PUBLISH_PROPERTY_PREVIEW';
const REQUEST_PUBLISH_PROPERTY = 'REQUEST_PUBLISH_PROPERTY';
const REQUEST_PUBLISH_PROPERTY_COMPLETE = 'REUEST_PUBLISH_PROPERTY_COMPLE';
const REQUEST_PUBLISH_PROPERTY_FAILED = 'REQUEST_PUBLISH_PROPERRY_FAILED';
const REQUEST_PRESALES = 'REQUEST_PRESALES';
const REQUEST_PRESALES_COMPLETE = 'REQUEST_PRESALES_COMPLETE';
const REQUEST_PRESALES_FAILED = 'REQUEST_PRESALES_FAILED';

const requestProperties = () => ({
  type: REQUEST_PROPERTIES,
});

const requestPropertiesComplete = (properties) => ({
  type: REQUEST_PROPERTIES_COMPLETE,
  payload: properties
});

const requestPropertiesFailed = (error) => ({
  type: REQUEST_PROPERTIES_FAILED,
  payload: error
});

const requestPresales = () => ({
  type: REQUEST_PRESALES,
});
const requestPresalesComplete = (presales) => ({
  type: REQUEST_PRESALES_COMPLETE,
  payload: presales
});

const requestPresalesFailed = (error) => ({
  type: REQUEST_PRESALES_FAILED,
  payload: error
});
const publishPropertyOne = (property) => ({
  type: PUBLISH_PROPERTY_ONE,
  payload: property
});
const publishPropertyPreview = (property) => ({
  type: PUBLISH_PROPERTY_PREVIEW,
  payload: property
});
const requestPublishProperty = (property) => ({
  type: REQUEST_PUBLISH_PROPERTY,
  payload: property,
});
const requestPublishPropertyComplete = (property) => ({
  type: REQUEST_PROPERTIES_FAILED,
  payload: property,
});
const requestPublishPropertyFailed = (error) => ({
  type: REQUEST_PUBLISH_PROPERTY_FAILED,
  payload: error
});

module.exports = {
  types: {
    REQUEST_PROPERTIES,
    REQUEST_PROPERTIES_COMPLETE,
    REQUEST_PROPERTIES_FAILED,
    REQUEST_PRESALES,
    REQUEST_PRESALES_COMPLETE,
    REQUEST_PRESALES_FAILED,
    PUBLISH_PROPERTY_ONE,
    PUBLISH_PROPERTY_PREVIEW,
    REQUEST_PUBLISH_PROPERTY,
    REQUEST_PUBLISH_PROPERTY_COMPLETE,
    REQUEST_PUBLISH_PROPERTY_FAILED
  },
  creators: {
    requestProperties,
    requestPropertiesComplete,
    requestPropertiesFailed,
    requestPresales,
    requestPresalesComplete,
    requestPresalesFailed,
    publishPropertyOne,
    publishPropertyPreview,
    requestPublishProperty,
    requestPublishPropertyComplete,
    requestPublishPropertyFailed
  }
}