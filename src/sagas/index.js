import {all, fork} from 'redux-saga/effects';
import {userSaga} from './User';
import propertySaga from './Properties';

export default function* rootSaga() {
  yield all([
    fork(userSaga),
    fork(propertySaga)
  ]);
}