import {takeLatest, select, put, call} from 'redux-saga/effects';
import {types as userTypes, creators as userCreators} from '../actions/User';
import {getSingleUser, userLogin, getUserProperties, newUser} from '../services';
import {Alert} from 'react-native';
import NavigationService from '../navigations/NavigationService';

const getSelectedUserId = ({userReducer}) => userReducer.selectedUserId;
const getUserLoginData = ({userReducer}) => ({email: userReducer.email, password: userReducer.password});
const getNewUserData = ({ userReducer }) => userReducer.newUser;
function* getUserSelectedProfile() {
  const id = yield select(getSelectedUserId);
  console.log(id);
  const user = yield call(getSingleUser, id);
  console.log(user);
  yield put(userCreators.requestUserProfileComplete(user));
}

function* doLogin() {
  const userData = yield select(getUserLoginData);
  const login = yield call(userLogin, userData);
  console.log(login);
  if (login.error) {
    yield put(userCreators.loginFailed(login.error));
    Alert.alert("Error", "Verifica tu información");
  } else {
    yield put(userCreators.loginComplete(login[0]));
    NavigationService.navigate("Tabs");
  }
}
function* createUser(){
  const newUserData = yield select(getNewUserData);
  console.log(newUserData);
  const newUserO = JSON.stringify({
    name: newUserData.nombre,
    lastName: newUserData.apellido,
    password: newUserData.password,
    email: newUserData.email,
    Provider: null,
    ID_Provider: null
  });
  console.log(newUserO);
 try{
  const request = yield call(newUser, newUserO);
  console.log(request);
  const login = yield call(userLogin, {
    email: newUserData.email,
    password: newUserData.password
  });
  yield put(userCreators.loginComplete(login[0]));
  yield put(userCreators.requestNewUserComplete(request[0]));
 } catch(e) {
   console.log(e);
   yield put(userCreators.requestNewUserFailed(true));
   alert('Error al registrarte vuelve a intentar');
 };
}


export function* userSaga() {
  yield takeLatest(userTypes.REQUEST_USER_PROFILE, getUserSelectedProfile);
  yield takeLatest(userTypes.REQUEST_EMAIL_LOGIN, doLogin);
  yield takeLatest(userTypes.REQUEST_NEW_USER, createUser);
}