import {call, put, select, takeLatest} from 'redux-saga/effects';
import {types as propertyType, creators as propertyCreators} from '../actions/Properties';
import {getPreventas, getProperties, publishPoperty} from '../services';
import NavigationService from '../navigations/NavigationService';

const getPropertyToPublish = ({propertyReducer}) => propertyReducer.propertyToPublish;

function* requestProperties() {
  const properties = yield call(getProperties);
  yield put(propertyCreators.requestPropertiesComplete(properties));
}

function* makeRequestPresales() {
  const response = yield call(getPreventas);
  console.log(response);
  yield put(propertyCreators.requestPresalesComplete(response.preventas));
}

function* requestPublishProperty() {
  const property = yield select(getPropertyToPublish);
  console.log(property);
  const response = yield call(publishPoperty, property);
  console.log(response);
  yield put(propertyCreators.requestPublishPropertyComplete(response));
  NavigationService.goBack();
  alert("Propiedad publicada correctamente.");
  yield put(propertyCreators.requestProperties());

}

export default function* propertySaga() {
  yield takeLatest(propertyType.REQUEST_PROPERTIES, requestProperties);
  yield takeLatest(propertyType.REQUEST_PRESALES, makeRequestPresales);
  yield takeLatest(propertyType.REQUEST_PUBLISH_PROPERTY, requestPublishProperty);
}