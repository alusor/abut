import React from 'react';
import {View, Text} from 'react-native';
import Header from '../components/Header';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import {LocaleConfig} from 'react-native-calendars';

LocaleConfig.locales['es'] = {
  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
  monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
  dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
  dayNamesShort: ['D', 'L', 'M', 'M', 'J', 'V', 'S']
};

LocaleConfig.defaultLocale = 'es';


export default () => (
  <View style={{flex: 1}}>
    <Header/>
    <View>
      <Calendar
        // Specify style for calendar container element. Default = {}
        style={{
          height: 350
        }}
        // Specify theme properties to override specific styles for calendar parts. Default = {}
        theme={{
          backgroundColor: '#ffffff',
          calendarBackground: '#ffffff',
          textSectionTitleColor: '#56ADE3',
          selectedDayBackgroundColor: '#00adf5',
          selectedDayTextColor: '#ffffff',
          todayTextColor: '#00adf5',
          dayTextColor: '#56ADE3',
          textDisabledColor: '#d9e1e8',
          dotColor: '#56ADE3',
          selectedDotColor: '#ffffff',
          arrowColor: 'orange',
          monthTextColor: '#56ADE3',
          textMonthFontWeight: 'bold',
          textDayFontSize: 16,
          textMonthFontSize: 16,
          textDayHeaderFontSize: 16
        }}
      />
    </View>
  </View>
);