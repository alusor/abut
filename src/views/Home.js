import React from 'react';
import {View, StyleSheet, FlatList, Text, ImageBackground, Dimensions, TouchableOpacity, Image} from 'react-native';
import Header from '../components/Header';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import {getProperties} from '../services';
import LoadingModal from '../components/LoadingModal';
import {connect} from 'react-redux';
import {creators as PropertyCreators} from '../actions/Properties';

const preventa = require('../icons/preventa.png');
const add = require('../icons/add.png');

class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      properties: [],
      isLoading: false
    };
    this.width = Dimensions.get('window').width;
  }

  async componentDidMount() {
    this.props.requestPresales();
    this.props.requestProperties();
    this.setState({
      isLoading: true,
      height: 0
    });
  }

  handleIndexChange = (index) => {
    this.setState({
      selectedIndex: index,
    });
  };
  renderItem = ({item}) => (
    <TouchableOpacity onPress={() => this.props.navigation.navigate('Detail', {property: item})}
                      style={{flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 5}}>
      <ImageBackground resizeMethod="resize" source={{uri: item.PropertyCoverPic}}
                       style={{height: this.width / 3 - 5, width: this.width / 2 - 5,}}>
        <View style={{position: 'absolute', bottom: 5, left: 5, right: 5}}>
          <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold'}}>{item.name}</Text>
          <Text style={{color: 'white', fontSize: 14}}>{item.Precio}</Text>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
  renderPresales = ({item}) => (
    <ImageBackground source={{uri: item.PresaleCoverPic}}
                     style={{width: '100%', height: this.state.height, justifyContent: 'flex-end'}}>
      <TouchableOpacity
        onPress={() => this.props.navigation.navigate('PresaleDetail', { presale: item })}
        style={{
        width: '100%',
        padding: 15,
        backgroundColor: 'rgba(255,255,255,0.31)',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <Image style={{width: 22, height: 22, marginHorizontal: 10}} source={add}/>
        <Text style={{color: 'white', fontSize: 18, fontWeight: 'bold', textAlign: 'center'}}>Info</Text>
      </TouchableOpacity>
    </ImageBackground>
  );

  render() {
    return (
      <View style={{flex: 1}}>
        <Header/>
        <View style={styles.white}>
          <View style={styles.segmented}>
            <SegmentedControlTab
              values={['Oportunidades', 'Pre-Ventas']}
              selectedIndex={this.state.selectedIndex}
              onTabPress={this.handleIndexChange}
              activeTabStyle={styles.activeTab}
              tabStyle={styles.tabStyle}
              tabTextStyle={styles.tabTextStyle}
            />
          </View>
        </View>
        <View onLayout={event => this.setState({height: event.nativeEvent.layout.height})} style={{flex: 1}}>
          {this.state.selectedIndex === 0 ?
            <View>
              <FlatList
                renderItem={this.renderItem}
                data={this.props.properties}
                keyExtractor={(item, index) => index}
                numColumns={2}
              />
              <LoadingModal visible={this.props.isLoading}/>
            </View> :
            <View>
              <FlatList
                key='presales'
                data={this.props.presales}
                renderItem={this.renderPresales}
                keyExtractor={(item, index) => index.toString() + '1e'}
              />
            </View>
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  white: {
    backgroundColor: 'white',
    padding: 10,
    alignItems: 'center'
  },
  segmented: {
    width: 280
  },
  activeTab: {
    backgroundColor: '#56ADE3'
  },
  tabStyle: {
    borderColor: '#56ADE3'
  },
  tabTextStyle: {
    color: '#56ADE3'
  }
});

const mapStateToProps = ({propertyReducer}) => ({
  isLoading: propertyReducer.isLoading,
  properties: propertyReducer.properties,
  presales: propertyReducer.presales
});

const dispatchToProps = (dispatch) => ({
  requestPresales: () => dispatch(PropertyCreators.requestPresales()),
  requestProperties: () => dispatch(PropertyCreators.requestProperties())
});

export default connect(mapStateToProps, dispatchToProps)(Home);