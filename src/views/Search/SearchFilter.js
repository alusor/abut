import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity, Text, Picker, TextInput, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import Header from '../../components/Header';
import SegmentedControlTab from "react-native-segmented-control-tab";
import Icon from 'react-native-vector-icons/Ionicons';
import Collapsible from 'react-native-collapsible';
import Card from "../../components/Card";
import Switch from '../../components/Switch';


class SearchFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      open: false
    };
  }

  handleIndexChange = (index) => {
    this.setState({
      selectedIndex: index,
    });
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header left leftAction={() => this.props.navigation.goBack()}/>
        <View style={{
          paddingVertical: 10,
          paddingHorizontal: 15,
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor: '#56ADE3'
        }}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Text style={{color: 'white', fontSize: 16}}>Cancelar</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Text style={{color: 'white', fontSize: 16}}>Aplicar</Text>
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={{paddingHorizontal: 20, paddingVertical: 10}}>
            <SegmentedControlTab
              values={['En Venta', 'En renta']}
              selectedIndex={this.state.selectedIndex}
              onTabPress={this.handleIndexChange}
              activeTabStyle={styles.activeTab}
              tabStyle={styles.tabStyle}
              tabTextStyle={styles.tabTextStyle}
            />
          </View>
          <View style={{
            marginHorizontal: 20,
            paddingVertical: 15,
            borderBottomColor: '#EBEBEB',
            borderBottomWidth: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}>
            <Text style={{color: '#56ADE3', fontSize: 16}}>Rango de precio</Text>
            <TextInput placeholder={'$5’000,000 - $10’000,000'}></TextInput>
          </View>
          <View style={{
            marginHorizontal: 20,
            paddingVertical: 15,
            borderBottomColor: '#EBEBEB',
            borderBottomWidth: 1,
            flexDirection: 'row',
            justifyContent: 'space-between'
          }}>
            <Text style={{color: '#56ADE3', fontSize: 16}}>Recámaras</Text>
            <View style={{elevation: 3, backgroundColor: 'white', borderRadius: 10}}>
              <Picker onValueChange={value => this.setState({recamara: value})} selectedValue={this.state.recamara}
                      style={{height: 30, width: 100,}}>
                <Picker.Item label="0" value="0"/>
                <Picker.Item label="1" value="1"/>
                <Picker.Item label="2" value="2"/>
                <Picker.Item label="3" value="3"/>
                <Picker.Item label="4" value="4"/>
                <Picker.Item label="5" value="5"/>
                <Picker.Item label="6" value="6"/>
                <Picker.Item label="+7" value="+7"/>
              </Picker>
            </View>
          </View>
          <View style={{marginHorizontal: 20, paddingVertical: 15, borderBottomColor: '#EBEBEB', borderBottomWidth: 1}}>
            <Text style={{color: '#56ADE3', fontSize: 16}}>Palabras clave</Text>
            <View>
              <TextInput style={{padding: 6, marginTop: 10, borderRadius: 6, borderColor: '#EBEBEB', borderWidth: 1,}}
                         placeholder='Colonia, alberca, cochera'/>
            </View>
          </View>
          <Collapsible collapsed={!this.state.open}>
            <View style={{
              marginHorizontal: 20,
              paddingVertical: 15,
              borderBottomColor: '#EBEBEB',
              borderBottomWidth: 1,
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
              <Text style={{color: '#56ADE3', fontSize: 16}}>Baños</Text>
              <View style={{elevation: 3, backgroundColor: 'white', borderRadius: 10}}>
                <Picker onValueChange={value => this.setState({banos: value})} selectedValue={this.state.banos}
                        style={{height: 30, width: 100,}}>
                  <Picker.Item label="0" value="0"/>
                  <Picker.Item label="1" value="1"/>
                  <Picker.Item label="2" value="2"/>
                  <Picker.Item label="3" value="3"/>
                  <Picker.Item label="4" value="4"/>
                  <Picker.Item label="5" value="5"/>
                  <Picker.Item label="6" value="6"/>
                  <Picker.Item label="+7" value="+7"/>
                </Picker>
              </View>
            </View>
            <View style={{
              marginHorizontal: 20,
              paddingVertical: 15,
              borderBottomColor: '#EBEBEB',
              borderBottomWidth: 1,
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
              <Text style={{color: '#56ADE3', fontSize: 16}}>Terreno m2</Text>
              <View style={{elevation: 3, backgroundColor: 'white', borderRadius: 10}}>
                <Picker onValueChange={value => this.setState({terreno: value})} selectedValue={this.state.terreno}
                        style={{height: 30, width: 100,}}>
                  <Picker.Item label="0" value="0"/>
                  <Picker.Item label="1" value="1"/>
                  <Picker.Item label="2" value="2"/>
                  <Picker.Item label="3" value="3"/>
                  <Picker.Item label="4" value="4"/>
                  <Picker.Item label="5" value="5"/>
                  <Picker.Item label="6" value="6"/>
                  <Picker.Item label="+7" value="+7"/>
                </Picker>
              </View>
            </View>
            <View style={{
              marginHorizontal: 20,
              paddingVertical: 15,
              borderBottomColor: '#EBEBEB',
              borderBottomWidth: 1,
              flexDirection: 'row',
              justifyContent: 'space-between'
            }}>
              <Text style={{color: '#56ADE3', fontSize: 16}}>Construccion m2</Text>
              <View style={{elevation: 3, backgroundColor: 'white', borderRadius: 10}}>
                <Picker onValueChange={value => this.setState({construccion: value})}
                        selectedValue={this.state.construccion} style={{height: 30, width: 100,}}>
                  <Picker.Item label="0" value="0"/>
                  <Picker.Item label="1" value="1"/>
                  <Picker.Item label="2" value="2"/>
                  <Picker.Item label="3" value="3"/>
                  <Picker.Item label="4" value="4"/>
                  <Picker.Item label="5" value="5"/>
                  <Picker.Item label="6" value="6"/>
                  <Picker.Item label="+7" value="+7"/>
                </Picker>
              </View>
            </View>
            <Card>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Text>Coto</Text>
                <Switch onValueChange={value => this.setState({coto: value})} value={this.state.coto} thumbColor='white'
                        trackColor={{true: '#56ADE3'}}></Switch>
              </View>
            </Card>
            <Card>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Text>Seguridad</Text>
                <Switch onValueChange={value => this.setState({seguridad: value})} value={this.state.seguridad}
                        thumbColor='#56ADE3' trackColor={{true: '#CCC'}}></Switch>
              </View>
            </Card>
            <Card>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Text>Jardín</Text>
                <Switch onValueChange={value => this.setState({jardin: value})} value={this.state.jardin}
                        thumbColor='#56ADE3' trackColor={{true: '#CCC'}}></Switch>
              </View>
            </Card>
            <Card>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Text>Alberca</Text>
                <Switch onValueChange={value => this.setState({alberca: value})} value={this.state.alberca}
                        thumbColor='#56ADE3' trackColor={{true: '#CCC'}}></Switch>
              </View>
            </Card>
            <Card>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Text>Cuarto de servicio</Text>
                <Switch onValueChange={value => this.setState({cuarto: value})} value={this.state.cuarto}
                        thumbColor='#56ADE3' trackColor={{true: '#CCC'}}></Switch>
              </View>
            </Card>
            <Card>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <Text>Casa de Huéspedes</Text>
                <Switch onValueChange={value => this.setState({casa: value})} value={this.state.casa}
                        thumbColor='#56ADE3' trackColor={{true: '#CCC'}}></Switch>
              </View>
            </Card>
          </Collapsible>
          <View>
            <TouchableOpacity onPress={() => this.setState({open: !this.state.open})} style={{
              padding: 20,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center'
            }}>
              <Text style={{
                color: '#56ADE3',
                fontSize: 16,
                textAlign: 'center',
                marginHorizontal: 10
              }}>{this.state.open ? 'Menos Filtros' : 'Más Filtros'}</Text>
              <Icon name={this.state.open ? 'ios-arrow-up' : 'ios-arrow-down'} size={30} color={'#56ADE3'}/>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  white: {
    backgroundColor: 'white',
    padding: 10,
    alignItems: 'center'
  },
  segmented: {
    width: 280
  },
  activeTab: {
    backgroundColor: '#56ADE3'
  },
  tabStyle: {
    borderColor: '#56ADE3'
  },
  tabTextStyle: {
    color: '#56ADE3'
  }
});

export default connect()(SearchFilter);