import React from 'react';
import {View, Text} from 'react-native';

const Divider = () => (
  <View style={{marginVertical: 10, width: '100%', flexDirection: 'row', alignItems: 'center'}}>
    <View style={{flex: 1, height: 1, backgroundColor: 'white'}}/>
    <Text style={{marginHorizontal: 30, color: 'white', fontWeight: 'bold', fontSize: 16}}>O</Text>
    <View style={{flex: 1, height: 1, backgroundColor: 'white'}}/>
  </View>
);

export default Divider;
