import React, {Component} from 'react';
import {
  View,
  ImageBackground,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Modal
} from 'react-native';
import LargeButton from '../../components/Button';

const city = require('../../icons/city.png');
const background = require('../../icons/background.png');
const logo = require('../../icons/abut_logo.png');

const arroba = require('../../icons/arro.png');
import LoadingModal from '../../components/LoadingModal';


export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      email: ''
    }
  }

  handleChangePassword = (text) => this.setState({password: text});
  recover = () => alert('Recuperar contraseña');

  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground style={styles.background} source={background}>
          <ImageBackground
            style={styles.city} source={city}>
            <ScrollView contentContainerStyle={{flex: 1, width: '100%', paddingTop: 50, alignItems: 'center'}}>
              <Image style={styles.logo} source={logo}/>

              <Text style={styles.socios}>
                Ingresa tu correo para recuperar tu contraseña
              </Text>
              <View style={styles.form}>
                <Image style={styles.icon} source={arroba}/>
                <TextInput value={this.state.email} onChangeText={this.handleChangeEmail} keyboardType='email-address'
                           placeholderTextColor='#9AB7DF' placeholder='email@tucorreo.com'
                           style={styles.input}></TextInput>
              </View>

              <LargeButton onPress={this.recover} color='#56ADE3' title='Recuperar'/>
              <LargeButton onPress={() => this.props.navigation.goBack()} color='#56ADE3' title='Cancelar'/>
            </ScrollView>
          </ImageBackground>
        </ImageBackground>
        <LoadingModal visible={this.state.isLoading}/>
      </View>
    );
  }
};


const styles = StyleSheet.create({
  background: {width: '100%', height: '100%'},
  city: {
    width: '100%',
    height: '100%',
  },
  logo: {height: 120, resizeMode: 'contain'},
  socios: {color: '#D1B100', fontWeight: 'bold', fontSize: 17, marginVertical: 15},
  form: {
    width: '90%',
    paddingVertical: 10,
    marginVertical: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomColor: 'white',
    borderBottomWidth: 1
  },
  icon: {width: 16, resizeMode: 'contain'},
  input: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    marginHorizontal: 5,
    flex: 1,
    margin: 0,
    padding: 0
  },
  content: {width: '90%'},
  password: {color: '#D1B100', fontWeight: 'bold', marginVertical: 15},
  nor: {fontSize: 20, fontWeight: 'bold', color: 'white'},
  ribbon: {
    width: '100%',
    backgroundColor: 'rgba(255,255,255,0.5)',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 15
  },
  socialRibbon: {width: '100%', backgroundColor: 'rgba(255,255,255,0.5)', alignItems: 'center', paddingVertical: 3}
});