import React, {Component} from 'react';
import {
  View,
  ImageBackground,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Modal,
  Alert
} from 'react-native';
import LargeButton from '../../components/Button';
import Divider from './Divider';

const FBSDK = require('react-native-fbsdk');
const {
  LoginManager,
} = FBSDK;
import {GoogleSignin} from 'react-native-google-signin';
import {connect} from 'react-redux';

const city = require('../../icons/city.png');
const background = require('../../icons/background.png');
const logo = require('../../icons/abut_logo.png');

const arroba = require('../../icons/arro.png');
const lock = require('../../icons/lock.png');
import LoadingModal from '../../components/LoadingModal';
import {userLogin} from '../../services';
import {creators} from '../../actions/User';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      email: '',
      password: ''
    }
  }

  componentDidMount() {
    GoogleSignin.configure();
  }

  requestLogin() {
    LoginManager.logInWithReadPermissions(['public_profile']).then(
      function (result) {
        if (result.isCancelled) {
          alert('Se cancelo el login');
        } else {
          alert('Login completo con los siguientes permisos: '
            + result.grantedPermissions.toString());
        }
      },
      function (error) {
        alert('Oops hubo un error: ' + error);
      }
    );
  }

  googleAuth() {
    GoogleSignin.signIn()
      .then((user) => {
        console.log(user);
        this.socialLogin(user, 'google');
      })
      .catch((err) => {
        console.log('WRONG SIGNIN', err);
      })
      .done();
  }

  handleChangeEmail = (text) => this.setState({email: text});
  handleChangePassword = (text) => this.setState({password: text});
  requestUserLogin = async () => {
    if (this.state.email.length !== 0 && this.state.password.length !== 0) {
      this.props.userLogin({
        email: this.state.email,
        password: this.state.password
      });
    } else {
      Alert.alert('Verifica tu información', 'Asegurate de ingresar tu correo electrónico y contraseña');
    }
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground style={styles.background} source={background}>
          <ImageBackground
            style={styles.city} source={city}>
            <ScrollView contentContainerStyle={{width: '100%', paddingTop: 50, alignItems: 'center'}}>
              <Image style={styles.logo} source={logo}/>

              <Text style={styles.socios}>
                Ingreso Socios
              </Text>
              <View style={styles.form}>
                <Image style={styles.icon} source={arroba}/>
                <TextInput value={this.state.email} onChangeText={this.handleChangeEmail} keyboardType='email-address'
                           placeholderTextColor='#9AB7DF' placeholder='email@tucorreo.com'
                           style={styles.input}></TextInput>
              </View>
              <View style={styles.form}>
                <Image style={styles.icon} source={lock}/>
                <TextInput value={this.state.password} onChangeText={this.handleChangePassword} secureTextEntry
                           placeholderTextColor='#9AB7DF' placeholder='contraseña' style={styles.input}></TextInput>
              </View>
              <View style={styles.content}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Password')}>
                  <Text style={styles.password}>Olvidaste tu Contraseña?</Text>
                </TouchableOpacity>
              </View>
              <LargeButton onPress={this.requestUserLogin} color='#56ADE3' title='Iniciar sesión'/>
              <Divider/>
              <View style={styles.socialRibbon}>
                <LargeButton icon='logo-facebook' onPress={this.requestLogin.bind(this)} color='#3D5A98'
                             title='Entrar con Facebook'/>
                <LargeButton icon='logo-google' onPress={this.googleAuth.bind(this)} color='#d34836'
                             title='Entrar con Google'/>

              </View>
              <View style={styles.ribbon}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Tabs')}>
                  <Text style={styles.nor}>Buscar sin registro</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </ImageBackground>
        </ImageBackground>
        <LoadingModal visible={this.props.isLoading}/>
      </View>
    );
  }
};


const styles = StyleSheet.create({
  background: {width: '100%', height: '100%'},
  city: {
    width: '100%',
    height: '100%',
  },
  logo: {height: 120, resizeMode: 'contain'},
  socios: {color: '#D1B100', fontWeight: 'bold', fontSize: 17, marginVertical: 15},
  form: {
    width: '90%',
    paddingVertical: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomColor: 'white',
    borderBottomWidth: 1
  },
  icon: {width: 16, resizeMode: 'contain'},
  input: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    marginHorizontal: 5,
    flex: 1,
    margin: 0,
    padding: 0
  },
  content: {width: '90%'},
  password: {color: '#D1B100', fontWeight: 'bold', marginVertical: 15},
  nor: {fontSize: 20, fontWeight: 'bold', color: 'white'},
  ribbon: {
    width: '100%',
    backgroundColor: 'rgba(255,255,255,0.5)',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 15
  },
  socialRibbon: {width: '100%', backgroundColor: 'rgba(255,255,255,0.5)', alignItems: 'center', paddingVertical: 3}
});

const mapStateToProps = ({userReducer}) => ({
  isLoading: userReducer.isLoading,
  error: userReducer.error
});

const mapDispatchToProps = (dispatch) => ({
  userLogin: user => dispatch(creators.requestEmailLogin(user))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);