import React from 'react';
import {View, ImageBackground, ActivityIndicator} from 'react-native';

const city = require('../../icons/city.png');
const background = require('../../icons/background.png');

export default (props) => (
  <View style={{flex: 1}}>
    <ImageBackground style={{width: '100%', height: '100%'}} source={background}>
      <ImageBackground style={{width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'}} conta
                       source={city}>
        <ActivityIndicator size='large' color='white'/>
      </ImageBackground>
    </ImageBackground>
  </View>
);
