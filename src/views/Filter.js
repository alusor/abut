import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {SafeAreaView} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

const Button = ({title}) => (
  <TouchableOpacity style={{
    paddingHorizontal: 5,
    paddingVertical: 10,
    borderBottomColor: '#C7C7CC',
    borderBottomWidth: StyleSheet.hairlineWidth
  }}>
    <Text style={{fontSize: 16}}>{title}</Text>
  </TouchableOpacity>
);

class Filter extends Component {
  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView>
          <View style={styles.header}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                              style={{width: 44, height: 44, justifyContent: 'center',}}>
              <Icon name="ios-arrow-back" color={'#56ADE3'} size={32}/>
            </TouchableOpacity>
            <View style={styles.headTitle}>
              <Text style={styles.white}>Filtros</Text>
            </View>
          </View>
        </SafeAreaView>
        <View style={styles.head}>
          <Text style={styles.headText}>FILTROS DE BUSQUEDA</Text>
        </View>
        <Button title='Ciudad'/>
        <Button title='Colonia'/>
        <Button title='Tipo de Operación'/>
        <Button title='Tipo de Propiedad'/>
        <Button title='Rango de Precios'/>
        <Button title='Rango de Metros Cuadrados'/>
        <TouchableOpacity style={{alignItems: 'flex-end', padding: 10}}>
          <Text style={{color: '#56ADE3', fontSize: 16}}>+ más Filtros</Text>
        </TouchableOpacity>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', padding: 10, alignItems: 'center'}}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Text style={{color: '#CC2F2F',}}>Limpiar Filtros</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{
            backgroundColor: '#56ADE3',
            paddingVertical: 5,
            paddingHorizontal: 10,
            borderRadius: 4,
          }}>
            <Text style={{
              color: 'white',
              textAlign: 'center'
            }}>Aplicar Filtros</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  white: {
    color: 'white',
    textAlign: 'center'
  },
  headTitle: {
    backgroundColor: '#56ADE3',
    padding: 5,
    borderRadius: 4,
    width: 80
  },
  head: {
    backgroundColor: '#EFEFF3',
    padding: 10,
    borderBottomColor: '#C7C7CC', borderBottomWidth: StyleSheet.hairlineWidth
  },
  headText: {
    color: '#6D6D72'
  }
});

export default connect()(Filter);