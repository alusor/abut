import styled from 'styled-components';

const Title = styled.Text`
    color: ${props => props.color ? props.color : '#56ADE3'};
    margin: 10px;
    font-size: 14px;
    flex: 1;
`;

export default Title;