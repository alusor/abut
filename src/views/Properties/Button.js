import React from 'react';
import styled from 'styled-components';

const Touchable = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
`;

const Icon = styled.Image`
    height: 64px;
    width: 64px;
    ${({selected}) => selected ? 'tint-color: white' : ''};
    ${({selected}) => selected ? 'background: #56ADE3' : ''};
    border-radius: 32px;
`;
const Title = styled.Text`
    text-align: center;
    font-size: 10px;
    margin-top: 5px;
    margin-bottom: 10px;
`;


const Button = (props) => (
  <Touchable selected={props.selected} onPress={props.onPress}>
    <Icon selected={props.selected} source={props.icon}/>
    <Title>{props.title}</Title>
  </Touchable>
);

export default Button;