import React, {Component} from 'react';
import {Text, View, Dimensions, Picker, TextInput, StyleSheet} from 'react-native';
import Title from './Title';
import Container from '../../components/Container';
import Row from '../../components/Row';
import PhotoContainer from './PhotoContainer';
import Card from '../../components/Card';
import LargeButton from '../../components/Button';
import Header from "../../components/Header";
import Switch from '../../components/Switch';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import {connect} from 'react-redux';
import {creators} from '../../actions/Properties';

class PropertyDetail extends Component {

  constructor(props) {
    super(props);
    this.height = Dimensions.get('window').width / 2 - 20;
    this.state = {
      recamara: 0,
      banos: 0,
      terreno: 0,
      construccion: 0,
      estacionamiento: 0,
      antiguedad: 0,
      descripcion: '',
      coto: false,
      seguridad: false,
      jardin: false,
      alberca: false,
      cuarto: false,
      casa: false,
      antiguedad: false,
      priceType: 0,
      precio: 0
    };
  }

  handleIndexChange = (index) => {
    this.setState({
      priceType: index,
    });
  };
  review = () => {
    this.props.goPreview(this.state);
    this.props.navigation.navigate("PropertyDataSheet");
  }

  render() {
    console.log(this.props);
    return (
      <View style={{flex: 1}}>
        <Header left leftAction={() => this.props.navigation.goBack()}/>
        <Container>
          <View style={{elevation: 5, backgroundColor: 'white', paddingBottom: 15,}}>
            <Row>
              <Title>Editar propiedad</Title>
              <Title color="#BBBBBB">Casa Habitación</Title>
            </Row>
            <Row style={{height: this.height, paddingHorizontal: 10}}>
              <PhotoContainer style={{marginRight: 10}}></PhotoContainer>
              <View style={{flex: 1}}>
                <Row style={{justifyContent: 'space-between'}}>
                  <PhotoContainer style={{marginRight: 5, marginBottom: 5}}></PhotoContainer>
                  <PhotoContainer style={{marginLeft: 5, marginBottom: 5}}></PhotoContainer>
                </Row>
                <Row>
                  <PhotoContainer style={{marginRight: 5, marginTop: 5}}></PhotoContainer>
                  <PhotoContainer style={{marginLeft: 5, marginTop: 5}}></PhotoContainer>
                </Row>
              </View>
            </Row>
          </View>
          <Title>Características de tu propiedad</Title>
          <Card>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center',}}>
              <View>
                <Text style={{color: '#56ADE3'}}>$</Text>
              </View>
              <TextInput value={this.state.precio} onChangeText={(text) => this.setState({precio: text})}
                         style={{flex: 1}} placeholder='Precio'></TextInput>
              <View style={styles.segmented}>
                <SegmentedControlTab
                  values={['Pesos MX', 'Dólares US']}
                  selectedIndex={this.state.priceType}
                  onTabPress={this.handleIndexChange}
                  activeTabStyle={styles.activeTab}
                  tabStyle={styles.tabStyle}
                  tabTextStyle={styles.tabTextStyle}
                />
              </View>
            </View>
          </Card>
          <Title>Información requerida</Title>
          <Card>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{flex: 1}}>Recámaras</Text>
              <View style={{elevation: 3, backgroundColor: 'white', borderRadius: 10}}>
                <Picker onValueChange={value => this.setState({recamara: value})} selectedValue={this.state.recamara}
                        style={{height: 30, width: 100,}}>
                  <Picker.Item label="0" value="0"/>
                  <Picker.Item label="1" value="1"/>
                  <Picker.Item label="2" value="2"/>
                  <Picker.Item label="3" value="3"/>
                  <Picker.Item label="4" value="4"/>
                  <Picker.Item label="5" value="5"/>
                  <Picker.Item label="6" value="6"/>
                  <Picker.Item label="+7" value="+7"/>
                </Picker>
              </View>
            </View>
          </Card>
          <Card>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{flex: 1}}>Baños</Text>
              <View style={{elevation: 3, backgroundColor: 'white', borderRadius: 10}}>
                <Picker onValueChange={value => this.setState({banos: value})} selectedValue={this.state.banos}
                        style={{height: 30, width: 100,}}>
                  <Picker.Item label="0" value="0"/>
                  <Picker.Item label="1" value="1"/>
                  <Picker.Item label="2" value="2"/>
                  <Picker.Item label="3" value="3"/>
                  <Picker.Item label="4" value="4"/>
                  <Picker.Item label="5" value="5"/>
                  <Picker.Item label="6" value="6"/>
                  <Picker.Item label="+7" value="+7"/>
                </Picker>
              </View>
            </View>
          </Card>
          <Card>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{flex: 1}}>Terreno</Text>
              <View style={{elevation: 3, backgroundColor: 'white', borderRadius: 10, width: 100}}>
                <TextInput value={this.state.terreno} onChangeText={(text) => this.setState({terreno: text})}
                           style={{paddingHorizontal: 10, paddingVertical: 3}} placeholder='00'></TextInput>
              </View>
            </View>
          </Card>
          <Card>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{flex: 1}}>Construcción</Text>
              <View style={{elevation: 3, backgroundColor: 'white', borderRadius: 10, width: 100}}>
                <TextInput value={this.state.construccion} onChangeText={(text) => this.setState({construccion: text})}
                           style={{paddingHorizontal: 10, paddingVertical: 3}} placeholder='00'></TextInput>
              </View>
            </View>
          </Card>
          <Card>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{flex: 1}}>Estacionamiento</Text>
              <View style={{elevation: 3, backgroundColor: 'white', borderRadius: 10}}>
                <Picker onValueChange={value => this.setState({estacionamiento: value})}
                        selectedValue={this.state.estacionamiento} style={{height: 30, width: 100,}}>
                  <Picker.Item label="0" value="0"/>
                  <Picker.Item label="1" value="1"/>
                  <Picker.Item label="2" value="2"/>
                  <Picker.Item label="3" value="3"/>
                  <Picker.Item label="4" value="4"/>
                  <Picker.Item label="5" value="5"/>
                  <Picker.Item label="6" value="6"/>
                  <Picker.Item label="+7" value="+7"/>
                </Picker>
              </View>
            </View>
          </Card>
          <Card>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{flex: 1}}>Antiguedad</Text>
              <View style={{elevation: 3, backgroundColor: 'white', borderRadius: 10}}>
                <Picker onValueChange={value => this.setState({antiguedad: value})}
                        selectedValue={this.state.antiguedad} style={{height: 30, width: 100,}}>
                  <Picker.Item label="0" value="0"/>
                  <Picker.Item label="1" value="1"/>
                  <Picker.Item label="2" value="2"/>
                  <Picker.Item label="3" value="3"/>
                  <Picker.Item label="4" value="4"/>
                  <Picker.Item label="5" value="5"/>
                  <Picker.Item label="6" value="6"/>
                  <Picker.Item label="+7" value="+7"/>
                </Picker>
              </View>
            </View>
          </Card>

          <Card>
            <Title>Descripción</Title>
            <TextInput value={this.state.descripcion} onChangeText={(descripcion) => this.setState({descripcion})}
                       placeholder="Descríbe detalladamente tu Propiedad y cuida tu ortografía" multiline={true}
                       style={{flex: 1}}></TextInput>
          </Card>
          <Title>Información opcional</Title>
          <Card>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <Text>Coto</Text>
              <Switch onValueChange={value => this.setState({coto: value})} value={this.state.coto} thumbColor='white'
                      trackColor={{true: '#56ADE3'}}></Switch>
            </View>
          </Card>
          <Card>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <Text>Seguridad</Text>
              <Switch onValueChange={value => this.setState({seguridad: value})} value={this.state.seguridad}
                      thumbColor='#56ADE3' trackColor={{true: '#CCC'}}></Switch>
            </View>
          </Card>
          <Card>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <Text>Jardín</Text>
              <Switch onValueChange={value => this.setState({jardin: value})} value={this.state.jardin}
                      thumbColor='#56ADE3' trackColor={{true: '#CCC'}}></Switch>
            </View>
          </Card>
          <Card>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <Text>Alberca</Text>
              <Switch onValueChange={value => this.setState({alberca: value})} value={this.state.alberca}
                      thumbColor='#56ADE3' trackColor={{true: '#CCC'}}></Switch>
            </View>
          </Card>
          <Card>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <Text>Cuarto de servicio</Text>
              <Switch onValueChange={value => this.setState({cuarto: value})} value={this.state.cuarto}
                      thumbColor='#56ADE3' trackColor={{true: '#CCC'}}></Switch>
            </View>
          </Card>
          <Card>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <Text>Casa de Huéspedes</Text>
              <Switch onValueChange={value => this.setState({casa: value})} value={this.state.casa} thumbColor='#56ADE3'
                      trackColor={{true: '#CCC'}}></Switch>
            </View>
          </Card>
          <View style={{justifyContent: 'center', alignItems: 'center', flex: 1, width: '100%'}}>
            <LargeButton onPress={this.review} color='#D1B100' title="Revisar ficha técnica"/>
          </View>
        </Container>
      </View>);
  }

}

const styles = StyleSheet.create({
  white: {
    backgroundColor: 'white',
    padding: 10,
    alignItems: 'center'
  },
  segmented: {
    width: 150,

  },
  activeTab: {
    backgroundColor: '#56ADE3'
  },
  tabStyle: {
    borderColor: '#56ADE3',
    width: 50,
  },
  tabTextStyle: {
    color: '#56ADE3',
    fontSize: 12
  }
});

const mapStateToProps = ({propertyReducer}) => ({
  publish: propertyReducer.publish
});

const mapDispatchToProps = (dispatch) => ({
  goPreview: (data) => dispatch(creators.publishPropertyPreview(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(PropertyDetail);
