import React, {Component} from 'react';
import Container from '../../components/Container';
import Card from '../../components/Card';
import Divider from './Divider';
import {Text, View, TextInput, StyleSheet, Alert} from 'react-native';
import {connect} from 'react-redux';
import Title from './Title';
import Button from './Button';
import CheckBox from '../../components/CheckBox';
import LargeButton from '../../components/Button';
import Input from '../../components/Input';
import Header from '../../components/Header';

import {creators} from '../../actions/Properties';

const casa = require('../../icons/casa.png');
const bodega = require('../../icons/bodega.png');
const comercial = require('../../icons/comercial.png');
const departamento = require('../../icons/departamento.png');
const oficina = require('../../icons/oficina.png');
const otro = require('../../icons/otro.png');
const rancho = require('../../icons/rancho.png');
const terreno = require('../../icons/terreno.png');

class PropertyPublish extends Component {
  constructor(props) {
    super(props);
    this.state = {
      venta: false,
      renta: false,
      transpaso: false,
      trueque: false,
      titulo: '',
      selected: -1
    };
  }

  checkType(type, currentValue) {

    this.setState({
      [type]: !currentValue
    });
  }

  title = (text) => {
    this.setState({
      titulo: text
    });
  }
  selectType = (type) => this.setState({selected: type});
  nextStep = () => {
    if(!this.props.user){
      Alert.alert('Error', 'Necesitas estar registrado para continuar');
      return;
    }
    if (this.state.titulo.length <= 10) {
      Alert.alert('Error', 'El titulo debe contener al menos 10 caracteres');
      return;
    }
    if (this.state.selected === -1) {
      Alert.alert('Error', 'Selecciona al menos un tipo de propiedad para continuar');
      return;
    }
    if (!this.state.venta && !this.state.renta && !this.state.transpaso && !this.state.trueque) {
      Alert.alert('Error', 'Selecciona al menos un tipo de trato que desees realizar para continuar.');
      return;
    }
    this.props.nextStep(this.state);
    this.props.navigation.navigate("PropertyDetails");
  }

  render() {
    console.log(this.state);
    return (
      <View style={{flex: 1}}>
        <Header/>
        <Container>
          <Title>
            Publicar tu propiedad
          </Title>
          <Card>
            <Input placeholder='Titulo de la propiedad' value={this.state.titulo} onChangeText={this.title}/>
          </Card>
          <Title>
            Tipo de propiedad
          </Title>
          <Divider/>
          <View style={{flexDirection: 'row', marginHorizontal: 10, marginTop: 10}}>
            <Button onPress={this.selectType.bind(this, 0)} selected={this.state.selected === 0} icon={casa}
                    title="Casa"></Button>
            <Button onPress={this.selectType.bind(this, 1)} selected={this.state.selected === 1} icon={departamento}
                    title="Departamento"></Button>
            <Button onPress={this.selectType.bind(this, 2)} selected={this.state.selected === 2} icon={terreno}
                    title="Terreno"></Button>
            <Button onPress={this.selectType.bind(this, 3)} selected={this.state.selected === 3} icon={oficina}
                    title="Oficina"></Button>
          </View>
          <View style={{flexDirection: 'row', marginHorizontal: 10}}>
            <Button onPress={this.selectType.bind(this, 4)} selected={this.state.selected === 4} icon={comercial}
                    title="Comercial"></Button>
            <Button onPress={this.selectType.bind(this, 5)} selected={this.state.selected === 5} icon={bodega}
                    title="Bodega"></Button>
            <Button onPress={this.selectType.bind(this, 6)} selected={this.state.selected === 6} icon={rancho}
                    title="Rancho"></Button>
            <Button onPress={this.selectType.bind(this, 7)} selected={this.state.selected === 7} icon={otro}
                    title="Otro"></Button>
          </View>
          <View style={{height: 1, backgroundColor: 'rgba(0,0,0,0.1)', width: '100%'}}/>
          <Text style={{margin: 10}}>
            Selecciona el tipo de operación que desea realizar.
          </Text>
          <Divider/>
          <View style={{flexDirection: 'row', marginHorizontal: 10}}>
            <CheckBox onPress={this.checkType.bind(this, 'venta', this.state.venta)} selected={this.state.venta}
                      color="#07B54C" title="Venta"/>
            <CheckBox onPress={this.checkType.bind(this, 'renta', this.state.renta)} selected={this.state.renta}
                      color="#E78B00" title="Renta"/>
            <CheckBox onPress={this.checkType.bind(this, 'transpaso', this.state.transpaso)}
                      selected={this.state.transpaso} color="#008AC4" title="Traspaso"/>
            <CheckBox onPress={this.checkType.bind(this, 'trueque', this.state.trueque)} selected={this.state.trueque}
                      color="#AA1265" title="Trueque"/>
          </View>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <LargeButton onPress={this.nextStep} title="Siguiente"/>
          </View>
        </Container>
      </View>
    );
  }
}

const mapStateToProps = ({userReducer}) => ({
  userReducer,
  user: userReducer.user
});

const mapDispatchToProps = (dispatch) => ({
  nextStep: (data) => dispatch(creators.publishPropertyOne(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(PropertyPublish);
