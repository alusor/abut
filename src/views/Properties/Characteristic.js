import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default (props) => (
  <View style={styles.content}>
    <Text style={styles.label}>{props.label}</Text>
    <Text style={styles.cont}>{props.content}</Text>
  </View>
);

const styles = StyleSheet.create({
  content: {
    marginHorizontal: 10,
    marginVertical: 5,
    flexDirection: 'row'
  },
  label: {
    fontSize: 18,
    flex: 2
  },
  cont: {
    fontSize: 18,
    flex: 1,
    color: 'black',
  }
});