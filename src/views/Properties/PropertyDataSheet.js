import React, {Component} from 'react';
import {View, Text, Image, ScrollView, StyleSheet, TouchableOpacity} from 'react-native';
import Lightbox from 'react-native-lightbox';
import Header from "../../components/Header";
import Carousel from '../../components/Carousel';
import Divider from '../../components/Divider';
import Chip from '../../components/Chip';
import Row from '../../components/Row';
import Characteristic from './Characteristic';
import PublishedProfile from '../../components/PublishedProfile';
import Icon from 'react-native-vector-icons/Ionicons';
import LargeButton from "../../components/Button";
import {connect} from 'react-redux';
import {creators} from '../../actions/Properties';


class PropertyDataSheet extends Component {
  state = {}

  publishProperty = () => {
    const {property, user} = this.props;
    const data = {
      "uid": user.id,
      "tProp": "1",
      "tEstilo": "1",
      "pName": property.titulo,
      "price": property.precio,
      "price2": property.precio,
      "currency": property.priceType + 1,
      "noBeds": property.recamara,
      "noBanC": property.banos,
      "noBanM": property.banos,
      "noCoch": property.estacionamiento,
      "mtsBuilt": property.construccion,
      "mtsLand": property.terreno,
      "units": "mt2",
      "Cap": "4",
      "builtIn": "2000",
      "tt_checkbox": ["1", "2"],
      "pa_checkbox": ["1", "2"],
      "Descr": property.descripcion,
      "Rules": "-",
      "st": "-",
      "noExt": "-",
      "noInt": "-",
      "colo": "1",
      "munip": "1",
      "edo": "1",
      "Pais": "1",
      "ZipC": "44160",
      "Disponible": "-",
      "DiasVisita": "-",
      "HorasVisita": "-",
      "stats": "-",
      "showDir": "FALSE",
      "lat": "20.6726488",
      "lng": "-103.3613684"
    }
    this.props.publish(data);
  };

  render() {
    console.log(this.props.property, this.props.user);
    const {property, user} = this.props;
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header left leftAction={() => this.props.navigation.goBack()}/>
        <ScrollView
          style={{flex: 1}}
        >
          <Lightbox>
            <Carousel/>
          </Lightbox>
          <Divider/>
          <Row style={styles.chipContainer}>
            {property.venta && <Chip color="#07B54C" title="Venta"/>}
            {property.renta && <Chip color="#E78B00" title="Renta"/>}
            {property.transpaso && <Chip color="#008AC4" title="Traspaso"/>}
            {property.trueque && <Chip color="#AA1265" title="Trueque"/>}
          </Row>
          <Divider/>
          <Text style={styles.title}>
            {property.titulo}
          </Text>
          <Row style={styles.center}>
            <Text style={styles.price}>Precio</Text>
            <Text style={styles.priceNumber}>${property.precio}</Text>
          </Row>
          <Divider/>
          <Row style={[styles.center, styles.content]}>
            <Text style={{width: 200}}>
              Buenos Aires 3984
              Col. Providencia
              Guadalajara, Jalisco
            </Text>
            <Icon size={32} name='md-pin' color='#56ADE3'/>
          </Row>
          <Divider/>
          <View>
            <Text style={styles.sectionTitle}>Características</Text>
            <Characteristic label='Precio' content={`$ ${property.precio}`}/>
            <Characteristic label='Recámaras' content={property.recamara}/>
            <Characteristic label='Baños' content={property.banos}/>
            <Characteristic label='Años de construida' content={property.antiguedad}/>
            <Characteristic label='Estacionamiento' content={property.estacionamiento}/>
            <Characteristic label='Terreno m2' content={property.terreno}/>
            <Characteristic label='Construcción m2' content={property.construccion}/>
            <Characteristic label='Jardín' content={property.jardin ? 'Si' : 'No'}/>
            <Characteristic label='Alberca' content={property.alberca ? 'Si' : 'No'}/>
            <Characteristic label='Coto' content={property.coto ? 'Si' : 'No'}/>
            <Characteristic label='Seguridad' content={property.seguridad ? 'Si' : 'No'}/>
            <Characteristic label='Casa de Huespedes' content={property.casa ? 'Si' : 'No'}/>
          </View>
          <Divider/>
          <View>
            <Text style={styles.sectionTitle}>Descripción</Text>
            <Text style={styles.description}>
              {property.descripcion}
            </Text>

          </View>
          <Divider/>
          <TouchableOpacity style={styles.whiteButton}>
            <Text style={styles.whiteButtonText}>Tour Virtual</Text>
          </TouchableOpacity>
          <Divider/>
          <TouchableOpacity style={styles.whiteButton}>
            <Text style={styles.whiteButtonText}>Como llegar</Text>
          </TouchableOpacity>
          <Divider/>
          <View>
            <Text style={styles.sectionTitle}>Publicado por</Text>
          </View>
          <PublishedProfile name={`${user.name} ${user.Apellido1}`} uPCount={user.cantidadPropiedades}/>
          <View style={styles.center}>
            <LargeButton onPress={this.publishProperty} title="Publicar Ficha Técnica"/>
          </View>
        </ScrollView>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  chipContainer: {
    justifyContent: 'space-around'
  },
  title: {
    color: 'black',
    fontSize: 21,
    margin: 10
  },
  price: {
    fontSize: 14,
    marginHorizontal: 10
  },
  priceNumber: {
    fontSize: 20,
    color: 'black',
  },
  center: {
    alignItems: 'center',
  },
  content: {
    justifyContent: 'space-between',
    marginHorizontal: 10
  },
  sectionTitle: {
    color: 'black',
    fontSize: 19,
    marginHorizontal: 10,
    marginVertical: 5
  },
  description: {
    marginHorizontal: 10,
    fontSize: 16,
    color: 'black'
  },
  tags: {
    marginHorizontal: 10,
    fontSize: 16,
    color: '#56ADE3'
  },
  whiteButton: {
    padding: 10
  },
  whiteButtonText: {
    fontSize: 16,
    color: '#56ADE3',
    textAlign: 'center'
  }
});

const mapStateToProps = ({propertyReducer, userReducer}) => ({
  property: propertyReducer.publish,
  user: userReducer.user
});

const mapDispatchToProps = (dispatch) => ({
  publish: (data) => dispatch(creators.requestPublishProperty(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(PropertyDataSheet);


/**
 * <Text style={styles.sectionTitle}>Tags</Text>
 <Text style={styles.tags}>Tags, son Palabras Clave para describir tu propiedad un
 poco mejor, separadas por comas, máximo 30 tags
 </Text>
 */