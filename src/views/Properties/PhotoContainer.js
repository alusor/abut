import React from 'react';
import styled from 'styled-components';

const foto = require('../../icons/foto.png');

const Container = styled.TouchableOpacity`
    flex-grow: 1;
    background-color: #E2F6FF;
    border-width: 1px;
    border-color: #95989A;
    justify-content: center;
    align-items: center;
`;

const Photo = styled.Image`
    resize-mode: contain;
    flex: 1;
    width: 44px;
    height: 44px;
`;

const PhotoContainer = ({style}) => (
  <Container style={style}>
    <Photo source={foto}></Photo>
  </Container>
);

export default PhotoContainer;
