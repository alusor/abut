import styled from 'styled-components';

const Divider = styled.View`
    height: 2px;
    background-color: #56ADE3;
    margin-horizontal: 10px;
`;

export default Divider;