import React from 'react';
import {View, StyleSheet, Image, Text, TextInput, ScrollView} from 'react-native';
import Header from '../components/Header';
import LargeButton from '../components/Button';
import Avatar from '../components/Avatar';
import {connect} from 'react-redux';

const header = require('../icons/header.png');
const foto = require('../icons/foto.png');
import {creators} from '../actions/User';
import LoadingModal from '../components/LoadingModal';
import Card from '../components/Card';


class OtherProfile extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const id = this.props.navigation.getParam('selectedUserID');
    console.log(id);
    this.props.getProfile(id);
  }

  render() {
    console.log(this.props);

    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header left leftAction={() => this.props.navigation.goBack()}/>
        {this.props.selectedUser.userdata && <View>
          <View style={styles.white}>
            <Image source={foto} resizeMode='contain' style={{width: 92, height: 92,}}/>
            <Avatar image={this.props.selectedUser.userdata.profilePic}/>
            <View style={{position: 'absolute', bottom: -50, left: 150}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={{fontSize: 12}}>{this.props.selectedUser.userdata.name} </Text>
                <Text style={{fontSize: 12}}>{this.props.selectedUser.userdata.Apellido1} </Text>
                <Text style={{fontSize: 12}}>{this.props.selectedUser.userdata.Apellido2}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={{fontSize: 12}}>{this.props.selectedUser.userdata.Municipio} </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={{
                  color: '#56ADE3',
                  fontSize: 12,
                }}>{this.props.selectedUser.userdata.cantidadPropiedades} propiedades</Text>
              </View>
            </View>
          </View>
          <Card>
            <Text style={styles.datos}>Descripción</Text>
            <Text>{this.props.selectedUser.userdata.Descrip}</Text>
          </Card>
          <Card>
            <Text style={styles.datos}>Información</Text>
            <View style={{flexDirection: 'row'}}>
              <Text>Teléfono: </Text><Text>{this.props.selectedUser.userdata.Tel}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text>Website: </Text><Text>{this.props.selectedUser.userdata.Website}</Text>
            </View>
          </Card>
        </View>}
        <LoadingModal visible={this.props.isLoading}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  white: {
    backgroundColor: '#E0E0E0',
    alignItems: 'center',
    justifyContent: 'center',
    height: 150,
    marginBottom: 60
  },
  datos: {
    color: '#56ADE3',
    fontSize: 16,
    paddingVertical: 10,

  },
  form: {
    padding: 15,
  },
  label: {
    color: '#A7A7A7',
    fontSize: 18
  },
  input: {
    fontSize: 16,
    paddingVertical: 5,
    marginBottom: 10,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 1
  }
});

const mapStateToProps = ({userReducer}) => ({
  selectedUser: userReducer.selectedUser,
  isLoading: userReducer.isLoading
});

const mapDispatchToProps = (dispatch) => ({
  getProfile: id => dispatch(creators.requestUserProfile(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(OtherProfile);