import React, {PureComponent} from 'react';
import {View} from 'react-native';
import Header from '../../components/Header';

export default class extends PureComponent {
  render() {
    return (
      <View>
        <Header left leftAction={() => this.props.navigation.goBack()}/>
      </View>
    );
  }
}