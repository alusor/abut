import React, {PureComponent} from 'react';
import {View, Text, Image, FlatList} from 'react-native';
import Header from '../../components/Header';
import Chip from '../../components/Chip';
import { connect } from 'react-redux';
import Button from '../../components/Button';

const Property = ({ item }) => (
  <View style={{flexDirection: 'row', backgroundColor: 'white', padding: 5, elevation: 5, marginBottom: 10}}>
    <View style={{flex: 1}}>
      <Text style={{fontSize: 16}}>
        {item.name}
      </Text>
      <Text style={{fontSize: 20}}>
        ${item.Precio}
      </Text>
      <View style={{flexDirection: 'row'}}>
        <Chip width='20px' color='#07B54C' title="V"></Chip>
        <Chip width='20px' title="R"></Chip>
        <Chip width='20px' title="T"></Chip>
        <Chip width='20px' title="T"></Chip>
      </View>
      <Text style={{fontSize: 12}}>
        {item.Calle} {item.no_Ext} Col. {item.Colonia}, {item.Municipio}, {item.Estado},
        CP. {item.CP}
      </Text>
    </View>
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Image resizeMethod="resize" source={{uri: item.PropertyCoverPic}} style={{width: 160, height: 120}}/>
    </View>
  </View>
);

class MyProperties extends PureComponent {
  render() {
    return (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <Header left leftAction={() => this.props.navigation.goBack()}/>
        {!this.props.user && <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Aún no estas registrado</Text>
            <Button onPress={() => this.props.navigation.navigate('Perfil')} title="Regístrate aqui" />
          </View>}
        {this.props.user && <FlatList
          data={this.props.properties}
          renderItem={({item}) => <Property item={item} /> }
          keyExtractor={(item, index) => index.toString()}
        /> }
      </View>
    );
  }
}

const mapStateToProps = ({ propertyReducer, userReducer }) => ({
  properties: propertyReducer.properties,
  user: userReducer.user
});

export default connect(mapStateToProps)(MyProperties);