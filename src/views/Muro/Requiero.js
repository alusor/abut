import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import {Row, Icon, Padding, TitleType, Title} from './Components';
import Divider from '../../components/Divider';
import Chip from '../../components/Chip';
import PublishedProfile from '../../components/PublishedProfile';

const icon = require('../../icons/requiero.png');
import Characteristic from '../Properties/Characteristic';

export default ({data, onProfile}) => (
  <View>
    <Divider/>
    <Row>
      <Padding>
        <Row>
          <Icon source={icon}/>
          <TitleType color='#0058FF'>Busco</TitleType>
        </Row>
      </Padding>
    </Row>
    <Divider/>
    <Title>
      {data.Titulo}
    </Title>
    <Padding>
      <Row style={styles.chipContainer}>
        <Chip color="#07B54C" title="Venta"/>
        <Chip color="#E78B00" title="Renta"/>
        <Chip color="#008AC4" title="Traspaso"/>
        <Chip color="#AA1265" title="Trueque"/>
      </Row>
    </Padding>
    <Divider/>
    <View style={{flex: 1}}>
      <Text style={{fontSize: 14, color: '#454545'}}>{data.Municipio} - {data.Estado}</Text>
      <Characteristic label='Presupuesto Máximo' content={`$${data.Precio_Hasta}`}/>
      <Characteristic label='Terreno' content={`${data.no_Mts_Cuadrados_Terreno}mts2`}/>
    </View>
    <Divider/>
    <Text style={styles.sectionTitle}>Descripción</Text>
    <Text style={styles.description}>{data.Descrip}</Text>
    <Divider/>
    <View>
      <Text style={styles.sectionTitle}>Publicado por</Text>
    </View>
    <PublishedProfile onPressProfile={onProfile} photo={data.OwnerProfilePic} name={data.uName} uPCount={data.uPCount}/>
  </View>
);

const styles = StyleSheet.create({
  price: {
    fontSize: 18,
    color: '#454545',
    marginHorizontal: 10
  },
  chipContainer: {
    justifyContent: 'space-around'
  },
  card: {
    width: '100%',
    height: 130,
    borderTopLeftRadius: 10,
    overflow: 'hidden'
  },
  priceNumber: {
    color: 'black',
    fontSize: 19,
    marginHorizontal: 10,
  },
  center: {
    alignItems: 'center'
  },
  sectionTitle: {
    color: 'black',
    fontSize: 19,
    marginHorizontal: 10,
    marginVertical: 5
  },
  description: {
    paddingHorizontal: 10,
    marginBottom: 5,
    color: '#454545'
  },
  sectionPublication: {
    fontSize: 14,
    marginHorizontal: 10,
    marginVertical: 5
  }
});