import React from 'react';
import styled from 'styled-components';

const Row = styled.View`
    flex-direction: row;
    align-items: center;
`;

const Icon = styled.Image`
    width: 32px;
    height: 32px;
`;

const Padding = styled.View`
    padding-horizontal: 10px;
    padding-vertical: 5px;
`;

const Title = styled.Text`
        color: black;
        font-size: 20;
        margin: 10px;
`;

const TitleType = styled.Text`
    font-size: 18;
    color: ${props => props.color};
    margin-horizontal: 5px;
`;

module.exports = {
  Row,
  Icon,
  Padding,
  TitleType,
  Title
};