import React from 'react';
import {View, Text, ScrollView, StyleSheet } from 'react-native';
import Lightbox from 'react-native-lightbox';
import Header from "../../components/Header";
import Carousel from '../../components/Carousel';
import Divider from '../../components/Divider';
import Row from '../../components/Row';
import Icon from 'react-native-vector-icons/Ionicons';

class PresaleDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  render() {
    const presale = this.props.navigation.getParam('presale');
    console.log(presale);
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header left leftAction={() => this.props.navigation.goBack()}/>
        <ScrollView>
          <Lightbox>
            <Carousel photos={presale.PresaleCoverPic}/>
          </Lightbox>
          <Text style={styles.title}>
            {presale.Nombre_Desarrollo}
          </Text>
          <Row style={styles.center}>
            <Text style={styles.price}>Precio desde</Text>
            <Text style={styles.priceNumber}>$ {presale.Precio_Desde} Hasta $ {presale.Precio_Hasta}</Text>
          </Row>
          <Divider/>
          <Row style={[styles.center, styles.content]}>
            <Text style={{width: 200}}>
              {presale.Calle} {presale.no_Ext} Col. {presale.Colonia}, {presale.Municipio}, {presale.Estado},
              CP. {presale.CP}
            </Text>
            <Icon size={32} name='md-pin' color='#56ADE3'/>
          </Row>
          <Divider/>
          <View>
            <Text style={styles.sectionTitle}>Descripción</Text>
            <Text style={styles.description}>{presale.Descrip}</Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  chipContainer: {
    justifyContent: 'space-around'
  },
  title: {
    color: 'black',
    fontSize: 21,
    margin: 10
  },
  price: {
    fontSize: 14,
    marginHorizontal: 10
  },
  priceNumber: {},
  center: {
    alignItems: 'center',
  },
  content: {
    justifyContent: 'space-between',
    marginHorizontal: 10
  },
  sectionTitle: {
    color: 'black',
    fontSize: 19,
    marginHorizontal: 10,
    marginVertical: 5
  },
  description: {
    margin: 10,
    fontSize: 14,
  },
  tags: {
    marginHorizontal: 10,
    fontSize: 14,
    color: '#56ADE3'
  },
  whiteButton: {
    padding: 10
  },
  whiteButtonText: {
    fontSize: 16,
    color: '#56ADE3',
    textAlign: 'center'
  }
});

export default PresaleDetail;