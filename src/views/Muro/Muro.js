import React, {Component} from 'react';
import {View, StyleSheet, Text, FlatList, TouchableOpacity, Modal} from 'react-native';
import {connect} from 'react-redux';
import Header from '../../components/Header';
import Container from '../../components/Container';
import Ofrezco from './Ofrezco';
import Requiero from './Requiero';
import {getOppotunityWall} from '../../services';
import LoadingModal from '../../components/LoadingModal';
import Button from '../../components/Button';


class Muro extends Component {

  constructor() {
    super();
    this.state = {
      loading: true,
      all_post: []
    };
  }

  async componentDidMount() {
    const data = await getOppotunityWall();
    console.log(data.all_posts);
    this.setState({
      loading: false,
      all_post: data.all_posts
    });
  }

  renderPost = ({item}) => {
    switch (item.id_Tipos_Status_Muro) {
      case "1":
        return <Ofrezco
          onProfile={() => this.props.navigation.navigate('OtherProfile', {selectedUserID: item.id_Usuario})}
          onPress={() => this.props.navigation.navigate('DetailP', {property: {id: item.id_Propiedad}})} data={item}/>
      case "2":
        return <Requiero
          onProfile={() => this.props.navigation.navigate('OtherProfile', {selectedUserID: item.id_Usuario})}
          data={item}/>
    }

  }

  keyExtractor = (item, index) => index.toString();

  render() {
    const registro = this.props.user? true: false;
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header/>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', padding: 15}}>
          <View/>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Filter')} style={{
            borderRadius: 4,
            borderWidth: 1,
            borderColor: '#454545',
            paddingVertical: 5,
            paddingHorizontal: 15
          }}>
            <Text style={{fontSize: 12}}>Filtros</Text>
          </TouchableOpacity>
        </View>
        {!registro &&           <View style={{ flex:1, justifyContent: 'center',
        alignItems: 'center', }}>
            <View style={{ backgroundColor: 'white', elevation: 4, borderRadius: 6, width: '90%', padding: 20, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ textAlign: 'center', marginBottom: 20, color: 'black', fontSize: 16 }}>Para poder ver este contenido necesitas ser usuario registrado.</Text>
              <Button onPress={() => this.props.navigation.navigate('Perfil')} title="Regístrate aqui" />
            </View>
          </View>}
        {registro && 
        <Container>
          <FlatList
            data={this.state.all_post}
            renderItem={this.renderPost}
            keyExtractor={this.keyExtractor}
          />
        </Container>
        }
        <LoadingModal visible={this.state.loading}/>        
      </View>
    );
  }
}

const mapStateToProps = ({ userReducer }) => ({
  user: userReducer.user
});

export default connect(mapStateToProps)(Muro);