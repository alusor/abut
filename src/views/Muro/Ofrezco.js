import React from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import {Row, Icon, Padding, TitleType, Title} from './Components';
import Divider from '../../components/Divider';
import Chip from '../../components/Chip';
import PublishedProfile from '../../components/PublishedProfile';

const icon = require('../../icons/ofrezco.png');

export default ({data, onPress, onProfile}) => (
  <View>
    <Divider/>
    <Row>
      <Padding>
        <Row>
          <Icon source={icon}/>
          <TitleType color='#CC2F2F'>Ofrezco</TitleType>
        </Row>
      </Padding>
    </Row>
    <Divider/>
    <Title>
      {data.Titulo}
    </Title>
    <Padding>
      <Row style={styles.center}>
        <Chip color='#07B54C' title='Venta'/>
        <Text style={styles.price}>Precio Reducido</Text>
      </Row>
    </Padding>
    <Divider/>
    <TouchableOpacity onPress={onPress}>
      <Row>
        <View style={{flex: 1}}>
          <Text style={styles.sectionPublication}>{data.propName}</Text>
          <Text style={styles.priceNumber}>${data.propPrice}</Text>
          <Text style={styles.description}>{data.propAddr}
          </Text>
        </View>
        <View style={{flex: 1, padding: 10}}>
          <View style={styles.card}>
            <Image style={styles.card} source={{uri: data.PropertyCoverPic || 'https://picsum.photos/200/200'}}/>
          </View>
        </View>
      </Row>
    </TouchableOpacity>
    <Divider/>
    <Text style={styles.sectionTitle}>Descripción</Text>
    <Text style={styles.description}>{data.Descrip}</Text>
    <Divider/>
    <View>
      <Text style={styles.sectionTitle}>Publicado por</Text>
    </View>
    <PublishedProfile onPressProfile={onProfile} photo={data.OwnerProfilePic} name={data.uName} uPCount={data.uPCount}/>
  </View>
);

const styles = StyleSheet.create({
  price: {
    fontSize: 18,
    color: '#454545',
    marginHorizontal: 10
  },
  card: {
    width: '100%',
    height: 130,
    borderTopLeftRadius: 10,
    overflow: 'hidden'
  },
  priceNumber: {
    color: 'black',
    fontSize: 19,
    marginHorizontal: 10,
  },
  center: {
    alignItems: 'center'
  },
  sectionTitle: {
    color: 'black',
    fontSize: 19,
    marginHorizontal: 10,
    marginVertical: 5
  },
  description: {
    paddingHorizontal: 10,
    marginBottom: 5,
    color: '#454545'
  },
  sectionPublication: {
    fontSize: 14,
    marginHorizontal: 10,
    marginVertical: 5
  }
});