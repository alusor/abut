import React from 'react';
import {View, Text, Image, ScrollView, StyleSheet, TouchableOpacity, Animated} from 'react-native';
import Lightbox from 'react-native-lightbox';
import Header from "../../components/Header";
import Carousel from '../../components/Carousel';
import Divider from '../../components/Divider';
import Chip from '../../components/Chip';
import Row from '../../components/Row';
import Characteristic from '../Properties/Characteristic';
import PublishedProfile from '../../components/PublishedProfile';
import Icon from 'react-native-vector-icons/Ionicons';
import {getProperty} from '../../services';
import LoadingModal from '../../components/LoadingModal';
import Orientation from 'react-native-orientation-locker';

class Detail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      orientation: 'PORTRAIT',
      rotate: new Animated.Value(1),
      open: false,
    };
  }

  async componentDidMount() {
    const data = await getProperty(this.props.navigation.getParam('property').id)
    this.setState({
      ...data.propertyproperties,
      loading: false,
    });
    Orientation.addOrientationListener(() => {
      Orientation.getDeviceOrientation(orientation => {
        if(!orientation.includes('PORTRAIT')) {
          this.setState({
            orientation
          });
        } else {
          this.setState({
            orientation: 'PORTRAIT'
          });
        }
        this.changeOrientation();
      });
    });
  }
  
  changeOrientation = () => {
    if(this.state.open) {
      const vale = this.state.orientation.includes('LANDSCAPE')? (this.state.orientation.includes('LEFT')? 2: 0): 1;
      Animated.timing(this.state.rotate, {
        toValue: vale,
        duration: 300
      }).start();
    }
  }
  resetOrientation = () => {
    Animated.timing(this.state.rotate, {
      toValue: 1,
      duration: 300
    }).start();
  };
  render() {
    const rotation = this.state.orientation === 'PORTRAIT'? {transform: [{rotate: '0deg'}]} : {transform: [{rotate: '90deg'}]};
    const rotate = this.state.rotate.interpolate({
      inputRange: [0, 1, 2],
      outputRange: ['-90deg', '0deg', '90deg'],
    });
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header left leftAction={() => this.props.navigation.goBack()}/>
        {!this.state.loading && <ScrollView
          style={{flex: 1}}
        >
          <Lightbox onOpen={() => this.setState({ open: true }, () => this.changeOrientation())} willClose={() => this.setState({ open: false }, () => this.resetOrientation())}>
            <Animated.View style={{ transform: [{ rotate: rotate }] }}>
            <Carousel photos={this.state.PropertyCoverPic}/>
            </Animated.View>
          </Lightbox>
          <Divider/>
          <Row style={styles.chipContainer}>
            <Chip color="#07B54C" title={this.state.Tipo_Trato}/>
          </Row>
          <Divider/>
          <Text style={styles.title}>
            {this.state.name}
          </Text>
          <Row style={styles.center}>
            <Text style={styles.price}>Precio</Text>
            <Text style={styles.priceNumber}>$ {this.state.Precio}</Text>
          </Row>
          <Divider/>
          <Row style={[styles.center, styles.content]}>
            <Text style={{width: 200}}>
              {this.state.Calle} {this.state.no_Ext} Col. {this.state.Colonia}, {this.state.Municipio}, {this.state.Estado},
              CP. {this.state.CP}
            </Text>
            <Icon size={32} name='md-pin' color='#56ADE3'/>
          </Row>
          <Divider/>
          <View>
            <Text style={styles.sectionTitle}>Características</Text>
            <Characteristic label='Precio' content={this.state.Precio}/>
            <Characteristic label='Recámaras' content={this.state.no_Recamaras}/>
            <Characteristic label='Baños completos' content={this.state.no_Banios_Completos}/>
            <Characteristic label='Baños medios' content={this.state.no_Banios_Medios}/>
            <Characteristic label='Capacidad de personas' content={this.state.Capacidad_de_Personas}/>
            <Characteristic label='Chocera' content={this.state.no_Cocheras}/>
            <Characteristic label='Mascotas' content={this.state.Mascotas === "1" ? 'Si' : 'No'}/>
            <Characteristic label='Terreno m2' content={this.state.no_Mts_Cuadrados_Terreno}/>
            <Characteristic label='Construcción m2' content={this.state.no_Mts_Cuadrados_Construccion}/>
            <Characteristic label='Seguridad' content={this.state.Seguridad == "0" ? 'No' : 'Si'}/>
            <Characteristic label='ID Propiedad' content={this.state.id}/>
          </View>
          <Divider/>
          <View>
            <Text style={styles.sectionTitle}>Descripción</Text>
            <Text style={styles.description}>
              {this.state.Descrip}
            </Text>
          </View>
          <Divider/>
          <TouchableOpacity style={styles.whiteButton}>
            <Text style={styles.whiteButtonText}>Tour Virtual</Text>
          </TouchableOpacity>
          <Divider/>
          <TouchableOpacity style={styles.whiteButton}>
            <Text style={styles.whiteButtonText}>Como llegar</Text>
          </TouchableOpacity>
          <Divider/>
          <View>
            <Text style={styles.sectionTitle}>Publicado por</Text>
          </View>
          <PublishedProfile
            onPressProfile={() => this.props.navigation.navigate('OtherProfile', {selectedUserID: this.state.id_usuario})}
            photo={this.state.OwnerProfilePic} name={this.state.Usuario} uPCount={this.state.uPCount}/>
        </ScrollView>}
        <LoadingModal visible={this.state.loading}/>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  chipContainer: {
    justifyContent: 'space-around'
  },
  title: {
    color: 'black',
    fontSize: 21,
    margin: 10
  },
  price: {
    fontSize: 14,
    marginHorizontal: 10
  },
  priceNumber: {
    fontSize: 20,
    color: 'black',
  },
  center: {
    alignItems: 'center',
  },
  content: {
    justifyContent: 'space-between',
    marginHorizontal: 10
  },
  sectionTitle: {
    color: 'black',
    fontSize: 19,
    marginHorizontal: 10,
    marginVertical: 5
  },
  description: {
    marginHorizontal: 10,
    fontSize: 16,
    color: 'black'
  },
  tags: {
    marginHorizontal: 10,
    fontSize: 16,
    color: '#56ADE3'
  },
  whiteButton: {
    padding: 10
  },
  whiteButtonText: {
    fontSize: 16,
    color: '#56ADE3',
    textAlign: 'center'
  }
});

export default Detail;