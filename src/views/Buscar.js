import React from 'react';
import {View, StyleSheet, TextInput, PermissionsAndroid, Platform, Text, Image, TouchableOpacity} from 'react-native';
import Header from '../components/Header';
import MapView, {Marker, Callout} from 'react-native-maps';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

async function requestLocationPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Permiso de ubicación',
        'message': 'Permisos de ubicación necesarios'
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera")
      return true;
    } else {
      console.log("Camera permission denied")
      return false;
    }
  } catch (err) {
    console.warn(err)
  }
}

class Buscar extends React.Component {

  constructor() {
    super();
    this.state = {
      search: '',
      region: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.315,
        longitudeDelta: 0.3121,
      }
    };
  }

  componentDidMount() {
    this.setLocation();
  }

  async setLocation() {
    const result = Platform.OS === 'ios' ? true : await requestLocationPermission();
    if (result) {
      navigator.geolocation.getCurrentPosition(position => {
        console.log(position);
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: this.state.region.latitudeDelta,
            longitudeDelta: this.state.region.longitudeDelta,
          }
        });
      })
    } else {
      alert("No pudimos obtener tu ubicacisón");
    }
  }

  render() {
    const markers = this.props.properties.map((marker, index) => {
      console.log(marker)
      return <Marker onLoad={() => this.forceUpdate()} key={index}
                     coordinate={{
                       latitude: parseFloat(marker.lat),
                       longitude: parseFloat(marker.lng)
                     }}
                     title={marker.name}
                     description={marker.Precio}
      >
        <Icon name='md-pin' color='#56ADE3' size={50}/>
        <Callout tooltip onPress={() => this.props.navigation.navigate('DetailMap', {property: marker})}>
          <View style={{
            width: 150,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 6,
            padding: 5,
            backgroundColor: 'white',
            elevation: 3
          }}>

            <Text style={{color: 'black', fontWeight: 'bold', textAlign: 'center'}}>{marker.name}</Text>
            <Text style={{textAlign: 'center'}}>${marker.Precio}</Text>
          </View>
        </Callout>
      </Marker>
    })
    return (
      <View style={{flex: 1}}>
        <Header/>
        <View style={styles.white}>
          <View style={styles.segmented}>
            <View style={styles.search}>
              <Icon name="md-search" color="white" size={16}/>
              <TextInput value={this.state.search} onChangeText={(text) => this.setState({search: text})}
                         onSubmitEditing={() => this.props.navigation.navigate('BuscarLista', {search: this.state.search})}
                         returnKeyType='search' style={styles.searchInput} placeholderTextColor='white'
                         placeholder='Buscar'/>
            </View>
          </View>
        </View>
        <View style={{backgroundColor: 'white', padding: 10, flexDirection: 'row'}}>
          <TouchableOpacity style={{flex: 1}} onPress={() => this.props.navigation.navigate('SearchFilter')}>
            <Text style={{color: '#56ADE3', textAlign: 'center'}}>Filtros</Text>
          </TouchableOpacity>
          <Text style={{color: '#56ADE3', textAlign: 'center'}}>|</Text>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('BuscarLista', {search: ''})}
                            style={{flex: 1}}>
            <Text style={{color: '#56ADE3', textAlign: 'center'}}>Lista</Text>
          </TouchableOpacity>
          <Text style={{color: '#56ADE3', textAlign: 'center'}}>|</Text>
          <TouchableOpacity style={{flex: 1}}>
            <Text style={{color: '#56ADE3', textAlign: 'center'}}>Tús busquedas</Text>
          </TouchableOpacity>
        </View>
        <MapView
          ref={MapView => (this.MapView = MapView)}
          initialRegion={this.state.region}
          loadingEnabled={true}
          loadingIndicatorColor="#666666"
          loadingBackgroundColor="#eeeeee"
          moveOnMarkerPress={false}
          showsUserLocation={true}
          showsMyLocationButton={true}
          showsCompass={true}
          showsPointsOfInterest={false}
          region={this.state.region}
          style={styles.map}>
          {markers}
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  white: {
    backgroundColor: '#56ADE3',
    padding: 10,
    alignItems: 'center'
  },
  map: {
    flex: 1,
  },
  segmented: {
    width: '100%'
  },
  activeTab: {
    backgroundColor: '#56ADE3'
  },
  tabStyle: {
    borderColor: '#56ADE3'
  },
  tabTextStyle: {
    color: '#56ADE3'
  },
  search: {
    padding: 0,
    margin: 0,
    backgroundColor: '#4896C6',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 6,
    flexDirection: 'row',
    alignItems: 'center'
  },
  searchInput: {
    padding: 0,
    margin: 0,
    color: 'white',
    paddingHorizontal: 10

  }
});

const mapStateToProps = ({propertyReducer}) => ({
  properties: propertyReducer.properties
});

export default connect(mapStateToProps)(Buscar);