import React from 'react';
import {View, StyleSheet, Image, Text, TextInput, ScrollView} from 'react-native';
import Header from '../components/Header';
import LargeButton from '../components/Button';
import Avatar from '../components/Avatar';
import Card from '../components/Card';
import {connect} from 'react-redux';
import {creators} from '../actions/User';
const header = require('../icons/header.png');
const foto = require('../icons/foto.png');


class Profile extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      nombre: '',
      apellido: '',
      email: '',
      password: '',
      password2: '',
    };
  }
  handleChange = (text, field) => {
    this.setState({
      [field]: text
    });
  }
  handleCreateUser = () => {
    if(this.state.nombre.length !== 0 && this.state.apellido.length !== 0 && this.state.email !== 0 && this.state.password !== 0 && this.state.password2 !== 0){
      this.props.createUser(this.state);
    } else {
      alert('Ingresa todos los datos');
    }
    
  };
  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <Header left leftAction={() => this.props.navigation.goBack()}/>
        {!this.props.user && <Register 
          handleChange={this.handleChange} 
          nombre={this.state.nombre}
          apellido={this.state.apellido}
          email={this.state.email}
          password={this.state.password}
          password2={this.state.password2}
          create={this.handleCreateUser}
          />}
        {this.props.user && <Current {...this.props.user}/>}
      </View>
    );
  }
}

const Current = (props) => (
  <View>
    <View style={styles.white}>
      <Image source={foto} resizeMode='contain' style={{width: 92, height: 92,}}/>
      <Avatar image={props.profilePic}/>
      <View style={{position: 'absolute', bottom: -50, left: 150}}>
        <View style={{flexDirection: 'row'}}>
          <Text style={{fontSize: 12}}>{props.name} </Text>
          <Text style={{fontSize: 12}}>{props.Apellido1} </Text>
          <Text style={{fontSize: 12}}>{props.Apellido2}</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={{fontSize: 12}}>{props.Municipio} </Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Text style={{color: '#56ADE3', fontSize: 12,}}>{props.cantidadPropiedades} propiedades</Text>
        </View>
      </View>
    </View>
    <Card>
      <Text style={styles.datos}>Descripción</Text>
      <Text>{props.Descrip}</Text>
    </Card>
    <Card>
      <Text style={styles.datos}>Información</Text>
      <View style={{flexDirection: 'row'}}>
        <Text>Teléfono: </Text><Text>{props.Tel}</Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        <Text>Website: </Text><Text>{props.Website}</Text>
      </View>
    </Card>
  </View>
);

const Register = ({
  nombre, apellido, email, password, password2, handleChange, create
}) => {
  const hname = (text) => handleChange(text, 'nombre');
  const hapellido = (text) => handleChange(text, 'apellido');
  const hemail = (text) => handleChange(text, 'email');
  const hpassword = (text) => handleChange(text, 'password');
  const hpassword2 = (text) => handleChange(text, 'password2');
  return (
    <View style={{ flex: 1 }}>
      <View style={styles.white}>
        <Image source={foto} resizeMode='contain' style={{width: 92, height: 92,}}/>
        <Avatar/>
      </View>
      <View>
        <Text style={styles.datos}>Ingresa tus datos para el registro</Text>
      </View>
      <ScrollView style={styles.form} contentContainerStyle={{paddingBottom: 30,}}>
        <Text style={styles.label}>
          Nombre
        </Text>
        <TextInput value={nombre} onChangeText={hname} style={styles.input}/>
        <Text style={styles.label}>
          Primer apellido
        </Text>
        <TextInput value={apellido} onChangeText={hapellido} style={styles.input}/>
        <Text style={styles.label}>
          Correo electrónico
        </Text>
        <TextInput value={email} onChangeText={hemail} style={styles.input}/>
        <Text style={styles.label}>
          Contraseña
        </Text>
        <TextInput secureTextEntry value={password} onChangeText={hpassword} style={styles.input}/>
        <Text style={styles.label}>
          Confirmar contraseña
        </Text>
        <TextInput secureTextEntry value={password2} onChangeText={hpassword2} style={styles.input}/>
      </ScrollView>
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <LargeButton onPress={create} title="Registrarme"/>
      </View>
    </View>
  );  
}

const styles = StyleSheet.create({
  white: {
    backgroundColor: '#E0E0E0',
    alignItems: 'center',
    justifyContent: 'center',
    height: 150,
    marginBottom: 60
  },
  datos: {
    color: '#56ADE3',
    fontSize: 16,
    padding: 10
  },
  form: {
    padding: 15,
  },
  label: {
    color: '#A7A7A7',
    fontSize: 18
  },
  input: {
    fontSize: 16,
    paddingVertical: 5,
    marginBottom: 10,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 1
  }
});

const mapStateToProps = ({userReducer}) => ({
  user: userReducer.user
});

const mapDispatchToProps = (dispatch) => ({
  getProfile: id => dispatch(creators.requestUserProfile(id)),
  createUser: user => dispatch(creators.requestNewUser(user)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

/**
 *     <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <LargeButton icon='logo-facebook' color='#3D5A98' title="Regístrate con Facebook"/>
    </View>
 */