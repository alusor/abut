const BASE_URL = 'http://api.myabut.com/public'

async function getProperties() {
  const response = await fetch(`${BASE_URL}/GetProperties`);
  const data = await response.json();
  return data.properties;
}

async function getProperty(id) {
  const response = await fetch(`${BASE_URL}/property`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "user_id": null,
      "property_id": id
    })
  });
  const data = await response.json();
  console.log(data.propertyproperties);
  return data;
}

async function getOppotunityWall() {
  const response = await fetch(`${BASE_URL}/loadOppotunityWall`);
  const data = await response.json();
  return data;
}

async function newUser(user) {
  console.log(user);
  const response = await fetch(`${BASE_URL}/users`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: user
  });
  const data = await response.json();
  return data;
}

async function userLogin(user) {
  const response = await fetch(`${BASE_URL}/users/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(user)
  });
  const data = await response.json();
  return data;
}

async function publishPoperty(property) {
  const response = await fetch(`${BASE_URL}/properties/add`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(property)
  });
  const data = await response.json();
  return data;
}

async function updateProperty(property) {
  const response = await fetch(`${BASE_URL}/properties/update`, {
    method: 'POST',
    body: property
  });
  const data = await response.json();
  return data;
}

async function getPreventas() {
  const response = await fetch(`${BASE_URL}/preventas`);
  const data = await response.json();
  return data;
}

async function getSingleUser(id) {
  const response = await fetch(`${BASE_URL}/users/single`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      user_id: id
    })
  });
  const data = await response.json();
  return data;
}

async function getUserProperties(id) {
  const response = await fetch(`${BASE_URL}/uproperties/${id}`);
  const data = await response.json();
  console.log(data);
  return true;
}

module.exports = {
  getProperties,
  getProperty,
  getOppotunityWall,
  userLogin,
  getPreventas,
  newUser,
  getSingleUser,
  getUserProperties,
  publishPoperty
};

/**
 * {
	"uid": "11",
	"tProp": "1", 
	"tEstilo": "1", 
	"pName": "Casa de prueba",
	"price": "7777777",
	"price2": "7777",
	"currency": "1", 
	"noBeds": "3",
	"noBanC": "3",
	"noBanM": "3",
	"noCoch": "3",
	"mtsBuilt": "55",
	"mtsLand": "45",
	"units": "mt2",
	"Cap": "4",
	"builtIn": "2000",
	"tt_checkbox": ["1","2"],
	"pa_checkbox": ["1","2"],	
	"Descr": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	"Rules": "-",
	"st": "Prisciliano Sánchez",
	"noExt": "1008",
	"noInt": "6",
	"colo": "1",
	"munip": "1",
	"edo": "1",
	"Pais": "1",
	"ZipC": "44160",
	"Disponible": "2018-05-20",
	"DiasVisita": "L-S",
	"HorasVisita": "09:00-18:00",
	"stats": "7",
	"showDir": "FALSE",
	"lat": "20.6726488",
	"lng": "-103.3613684"
}
 */