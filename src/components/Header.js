import React, {PureComponent, Fragment} from 'react';
import {StatusBar, Platform, View, Animated, Text, Dimensions, Image} from 'react-native';
import {SafeAreaView} from 'react-navigation';
import styled from 'styled-components';
import Icon from 'react-native-vector-icons/Ionicons';
import NavigationService from '../navigations/NavigationService';

const icon = require('../icons/abut_icon.png');

const menu = [
  {text: 'Mis Propiedades', icon: require('../icons/control/mis_propiedades.png'), route: 'MyProperties'},
  {text: 'Notificaciones', icon: require('../icons/control/notificaciones.png'), route: 'Blank'},
  {text: 'Chat', icon: require('../icons/control/chat.png'), route: 'Blank'},
  {text: 'Mi Cuenta', icon: require('../icons/control/mi_cuenta.png'), route: 'Perfil'},
  {text: 'Publica', icon: require('../icons/control/publica.png'), route: 'Publicar'},
  {text: 'Mis Estadísticas', icon: require('../icons/control/mis_propiedades.png'), route: 'Blank'},
  {text: 'Blog', icon: require('../icons/control/blog.png'), route: 'Blank'},
  {text: 'Compartir', icon: require('../icons/control/compartir.png'), route: 'Blank'},
  {text: 'Configuración', icon: require('../icons/control/configuracion.png'), route: 'Blank'},
];

const Safe = styled(SafeAreaView)`
    background-color: ${props => props.color ? props.color : '#56ADE3'};
    position: relative;
    top: 0;
    left: 0;
    right: 0;
    padding-vertical: ${Platform.OS === 'android' ? 6 : 0};
    position: relative;
    z-index: 3000000;
`;
const Title = styled.Text`
    color: white;
    font-size: 20px;
    text-align: center;
    flex: 1;
`;
const Content = styled.View`
    background-color: ${props => props.color ? props.color : '#56ADE3'};

    flex-direction: row;
    justify-content: center;
    align-items: center;
    height: 44px;
    z-index: 3000000;
`;
const ActionContent = styled.View`
    width: 20%;
    height: 44px;
    align-items: ${({menu}) => !menu ? 'flex-start' : 'flex-end'};
    
`;
const Action = styled.TouchableOpacity`
    height: 44px;
    width: 44px;
    justify-content: center;
    align-items: center;
`;
const Abut = styled.Image`
    width: 40px;
    height: 40px;
`;

class Header extends PureComponent {
  constructor(props) {
    super(props);
    this.open = new Animated.Value(1);
    this.state = {
      menu: false
    }
  }

  openMenu = () => {
    this.setState({menu: !this.state.menu}, () => {
      Animated.timing(this.open, {
        toValue: this.state.menu ? 0 : 1
      }).start();
    });
  }
  navigate = (screen) => {
    this.setState({menu: !this.state.menu}, () => {
      Animated.timing(this.open, {
        toValue: this.state.menu ? 0 : 1
      }).start(() => NavigationService.navigate(screen));
    });
    //this.setState({ menu: !this.state.menu }, () => );

  }

  render() {
    const {props} = this;
    const position = this.open.interpolate({
      inputRange: [0, 1],
      outputRange: [0, -600]
    });
    return (
      <Fragment>
        <Safe color={props.color}>
          <StatusBar backgroundColor='#56ADE3' barStyle="light-content"/>
          <Content>
            <ActionContent>
              {props.left ? <Action onPress={props.leftAction}><Icon name="ios-arrow-back" size={30}
                                                                     color="white"/></Action> : null}
            </ActionContent>
            <View style={{flex: 1, alignItems: 'center',}}>
              {props.title ? <Title>{props.title}</Title> : <Abut source={icon}/>}
            </View>
            <ActionContent menu>
              <Action onPress={this.openMenu}><Icon name="md-menu" size={30} color='white'/></Action>
            </ActionContent>

          </Content>
        </Safe>
        <Animated.View style={{
          transform: [{translateY: position}],
          width: '100%',
          backgroundColor: 'rgba(255,255,255,0.9)',
          position: 'absolute',
          zIndex: 20
        }}>
          <SafeAreaView>
            <Text style={{marginTop: 74, textAlign: 'center', fontSize: 20}}>PANEL DE CONTROL</Text>
            <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap'}}>
              {menu.map((button, index) => (
                <ButtonContainer key={index}>
                  <Button onPress={this.navigate.bind(this, button.route)} text={button.text} icon={button.icon}
                          key={index}/>
                </ButtonContainer>
              ))}
            </View>
          </SafeAreaView>
        </Animated.View>

      </Fragment>
    );
  };

}

export default Header;

const ButtonContainer = styled.View`
    width: ${Dimensions.get('window').width / 3}; 
    height: ${Dimensions.get('window').width / 3};
    padding: 5px; 
`;

const ButtonStyled = styled.TouchableOpacity`
    padding: 8px;
    flex: 1;
    margin: 5px; 
    background-color: white;
    border-radius: 5px;
    box-shadow: 2px 1px 5px rgba(0,0,0,0.16);
    elevation: 3;
`;

const Button = (props) => (
  <ButtonStyled onPress={props.onPress}>
    <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
      <Image style={{width: 44, height: 38, resizeMode: 'contain',}} source={props.icon}/>
    </View>
    <Text style={{textAlign: 'center', fontSize: 12}}>{props.text}</Text>
  </ButtonStyled>
); 