import React from 'react';
import {View, Image, TouchableOpacity, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Divider from './Divider';

export default (props) => (
  <View>
    <View style={styles.container}>
      <View style={styles.avatarContainer}>
        <Image source={{uri: props.photo || 'https://picsum.photos/200/200'}} style={styles.avatar}/>
      </View>
      <View style={styles.dataContainer}>
        <Text style={styles.name}>{props.name || 'Silvy Vela M.'}</Text>

        <Text style={styles.properties}>{props.uPCount || '0'} propiedades</Text>
      </View>
      <View style={styles.buttons}>
        <TouchableOpacity style={styles.chatButton}>
          <View style={styles.circle}>
            <Icon size={22} name='md-mail' color='white'/>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={props.onPressProfile} style={styles.seeProfile}>
          <Text>Ver perfil</Text>
          <Icon size={14} name='ios-arrow-forward' color='gray'/>
        </TouchableOpacity>
      </View>
    </View>
    <Divider/>
  </View>
);

const styles = StyleSheet.create({
    container: {
      flexDirection: 'row'
    },
    avatarContainer: {
      backgroundColor: 'white',
      elevation: 5,
      margin: 10,
      borderRadius: 38,
      width: 76,
      height: 76,
      justifyContent: 'center',
      alignItems: 'center'
    },
    avatar: {
      width: 72,
      height: 72,
      borderRadius: 36,
      backgroundColor: 'white',
    },
    dataContainer: {
      flex: 1,
      marginHorizontal: 15
    },
    name: {
      fontSize: 17,
      color: 'black'
    },
    location: {
      fontSize: 16,
      color: 'black'
    },
    fro: {
      fontSize: 14,
      color: 'black'
    },
    properties: {
      fontSize: 14,
      color: '#56ADE3'
    },
    chatButton: {
      backgroundColor: '#56ADE3',
      width: 75,
      height: 50,
      borderRadius: 6,
      justifyContent: 'center',
      alignItems: 'center'
    }, circle: {
      borderColor: 'white',
      width: 32,
      height: 32,
      borderRadius: 16,
      borderWidth: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    seeProfile: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    buttons: {
      marginHorizontal: 10,
      justifyContent: 'space-around'
    }
  }
);

/**
 *
 *                 <Text style={styles.location}>Guadalajara, Jalisco</Text>
 <Text style={styles.fro}>Inmobiliaria MDR</Text>
 */