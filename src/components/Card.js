import styled from 'styled-components';

const Card = styled.View`
    elevation: 3;
    background-color: white;
    padding: 10px;
    margin-vertical: 5px;
`;

export default Card;
