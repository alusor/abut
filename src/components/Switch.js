import React from 'react';
import {Switch, View, Text} from 'react-native';

export default (props) => (
  <View style={{flexDirection: 'row', alignItems: 'center'}}>
    <Text style={{fontSize: 10}}>No / Si </Text>
    <Switch onValueChange={props.onValueChange} value={props.value} thumbColor='white'
            trackColor={{true: '#56ADE3'}}></Switch>
  </View>

);
