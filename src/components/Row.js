import React from 'react';
import styled from 'styled-components';

const Row = styled.View`
    flex-direction: row;
    flex: 1;
`;

export default Row;