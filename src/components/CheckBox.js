import React from 'react';
import styled from 'styled-components';
import Chip from './Chip';

const checked = require('../icons/check.png');

const Container = styled.View`
    flex: 1;
    margin: 5px;
    justify-content: center;
    align-items: center;
`;
const Box = styled.TouchableOpacity`
    height: 54px;
    margin-vertical: 5px;
    width: 64px;
    border-radius: 5px;
    border-width: 3px;
    border-color: #56ADE3;
`;
const BoxChecked = styled.TouchableOpacity`
    height: 54px;
    margin-vertical: 5px;
    width: 64px;
    border-radius: 5px;
    border-width: 3px;
    border-color: #56ADE3;
    background-color: #56ADE3;
    justify-content: center;
    align-items: center;
`;
const Image = styled.Image`
    height: 34px;
    resize-mode: contain;
`;
const CheckBox = (props) => (
  <Container>
    {!props.selected ? <Box onPress={props.onPress}/> :
      <BoxChecked onPress={props.onPress}><Image source={checked}></Image></BoxChecked>}
    <Chip color={props.color} title={props.title}></Chip>
  </Container>
)

export default CheckBox;