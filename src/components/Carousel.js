import React from 'react';
import {Image} from 'react-native';
import Swiper from 'react-native-swiper';

const Slider = (props) => (

  <Image resizeMethod='resize' source={{uri: props.photos}} style={[{width: '100%', height: 250}, props.style]}/>

);

export default Slider;

/**
 *
 *
 *     <Swiper
 autoplay width={'100%'}
 height={250}
 activeDotColor='white'
 dotColor='rgba(255,255,255,.5)'
 paginationStyle={{ margin: 0, bottom: 7 }}
 >
 </Swiper>
 */