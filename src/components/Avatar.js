import React from 'react';
import styled from 'styled-components';

const foto = require('../icons/foto.png');

const Image = styled.Image`
    width: 92px;
    height: 92px;
    border-radius: 46px;
`;

const PlaceHolder = styled.Image`
    width: 44px;
    height: 44px;
    resize-mode: contain;
`;

const Container = styled.TouchableOpacity`
    width: 92px;
    height: 92px;
    border-radius: 46px;
    justify-content: center;
    align-items: center;
    overflow: hidden;
    position: absolute;
    bottom: -55;
    border-color: black;
    border-width: 1px;
    left: 46;
    background: #E0E0E0;
`;

const Avatar = (props) => (
  <Container>
    {props.image && <Image source={{uri: props.image}}/>}
    {!props.image && <PlaceHolder source={foto}/>}
  </Container>
);

export default Avatar;