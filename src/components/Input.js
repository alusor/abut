import React from 'react';
import {TextInput, Text, View, TouchableOpacity, Image} from 'react-native';

const info = require('../icons/info.png');

const Input = (props) => (
  <View>
    <View style={styles.container}>
      <TextInput style={styles.input} placeholder={props.placeholder} value={props.value}
                 onChangeText={props.onChangeText}/>
      <TouchableOpacity>
        <Image style={styles.info} source={info}/>
      </TouchableOpacity>
    </View>
    <View style={styles.container}>
      <Text style={styles.text}>Minimo de 10 letras</Text>
      <Text style={styles.text}>{`${props.value.length} / 200`}</Text>
    </View>
  </View>
);

const styles = {
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  text: {
    fontSize: 12
  },
  info: {
    width: 22,
    height: 22,
  },
  input: {
    flex: 1,
    margin: 0,
    padding: 0,
    paddingVertical: 5
  }
};

export default Input;
