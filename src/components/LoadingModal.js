import React from 'react';
import {View, Modal, ActivityIndicator} from 'react-native';

const LoadingModal = ({visible}) => (
  <Modal
    transparent
    visible={visible}
    onRequestClose={() => null}
  >
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.3)'}}>
      <View style={{backgroundColor: 'white', borderRadius: 6, padding: 15}}>
        <ActivityIndicator color='#56ADE3' size='large'/>
      </View>
    </View>
  </Modal>
);


export default LoadingModal;