import React from 'react';

import styled from 'styled-components';

const ChipContainer = styled.View`
    background-color: ${props => props.color ? props.color : '#56ADE3'};
    padding-horizontal: 5px;
    padding-vertical: 3px;
    border-radius: 15px;
    width: ${props => props.width ? props.width : '80px'};
    ${props => props.width ? 'marginHorizontal: 5px' : 'marginHorizontal: 0px'};


`;
const Title = styled.Text`
    color: white;
    text-align: center;
    font-size: 10px;
`;
const Chip = (props) => (
  <ChipContainer width={props.width} color={props.color}>
    <Title numberOfLines={1}>{props.title.toUpperCase()}</Title>
  </ChipContainer>
);

export default Chip;