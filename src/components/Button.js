import React from 'react';
import styled from 'styled-components';
import Icon from 'react-native-vector-icons/Ionicons';

const Touchable = styled.TouchableOpacity`
    margin: 5px;
    background-color: ${props => props.color ? props.color : '#07B54C'};
    border-radius: 6px;
    elevation: 3;
    padding: 7px;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    width: 300px;

`;

const Title = styled.Text`
    text-align: center;
    font-size: 16px;
    color: white;
    font-weight: bold;
    margin: 5px;
`;
  

const Button = (props) => (
  <Touchable color={props.color} onPress={props.onPress}>
    {props.icon ? <Icon name={props.icon} size={24} color='white'/> : null}
    <Title>{props.title}</Title>
  </Touchable>
);

export default Button;