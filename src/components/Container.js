import styled from 'styled-components';

const Container = styled.ScrollView`
    background-color: white;
    flex: 1;
`;

export default Container;