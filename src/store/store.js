import {createStore, applyMiddleware, compose} from 'redux';
import rootReducer from '../reducers';
import createSagaMiddleware from 'redux-saga';
import sagas from '../sagas';

const sagaMiddleware = createSagaMiddleware();
const middleware = [
  sagaMiddleware
];
const enhancers = [];
const initialState = {};
const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

const Store = createStore(rootReducer, initialState, composedEnhancers);
sagaMiddleware.run(sagas);

export default Store;