import {combineReducers} from 'redux';
import userReducer from './User';
import propertyReducer from './Properties';

const rootReducer = combineReducers({
  userReducer,
  propertyReducer
});

export default rootReducer;