import {types} from '../actions/User';

const initialState = {
  user: null,
  selectedUserId: null,
  currentUserId: null,
  selectedUser: {},
  isLoading: false,
  newUser: null
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.REQUEST_USER_PROFILE:
      return {...state, selectedUserId: action.payload, isLoading: true};
    case types.REQUEST_USER_PROFILE_COMPLETE:
      return {...state, selectedUser: action.payload, isLoading: false};
    case types.REQUEST_USER_PROFILE_FAILED:
      return {...state, error: action.payload, isLoading: false};
    case types.REQUEST_EMAIL_LOGIN:
      return {...state, email: action.payload.email, password: action.payload.password, isLoading: true};
    case types.LOGIN_COMPLETE:
      return {...state, user: action.payload, isLoading: false};
    case types.LOGIN_FAILED:
      return {...state, error: action.payload, isLoading: false, user: null};
    case types.REQUEST_NEW_USER:
      return { ...state, isLoading: true, newUser: action.payload };
    case types.REQUEST_NEW_USER_COMPLETE:
      return { ...state, isLoading: false, newUser: null };
    case types.REQUEST_NEW_USER_FAILED:
      return { ...state, isLoading: false, newUser: null };
    default:
      return state;
  }
};

export default userReducer;