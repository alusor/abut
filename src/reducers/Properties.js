import {types} from '../actions/Properties';

const initialState = {
  isLoading: false,
  properties: [],
  presales: [],
  error: false,
  publish: {},
  propertyToPublish: {}
};

const propertyReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.REQUEST_PROPERTIES:
      return {...state, isLoading: true};
    case types.REQUEST_PROPERTIES_COMPLETE:
      return {...state, isLoading: false, properties: action.payload,};
    case types.REQUEST_PROPERTIES_FAILED:
      return {...state, isLoading: false, error: true};
    case types.REQUEST_PRESALES:
      return {...state, isLoading: true};
    case types.REQUEST_PRESALES_COMPLETE:
      return {...state, isLoading: false, presales: action.payload,};
    case types.REQUEST_PRESALES_FAILED:
      return {...state, isLoading: false, error: true};
    case types.PUBLISH_PROPERTY_ONE:
      return {...state, publish: {...state.publish, ...action.payload}};
    case types.PUBLISH_PROPERTY_PREVIEW:
      return {...state, publish: {...state.publish, ...action.payload}};
    case types.REQUEST_PUBLISH_PROPERTY:
      return {...state, isLoading: true, propertyToPublish: action.payload};
    case types.REQUEST_PUBLISH_PROPERTY_COMPLETE:
      return {...state, isLoading: false, propertyToPublish: {}, publish: {},};
    case types.REQUEST_PUBLISH_PROPERTY_FAILED:
      return {...state, isLoading: false, error: payload.error};
    default:
      return state;
  }
}

export default propertyReducer;