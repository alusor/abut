import {types} from '../actions/Properties';

const initialState = {
  isLoading: false,
  wall: {},
  error: false
};

const propertyReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.REQUEST_OPORTNITY_WALL:
      return {...state, isLoading: true};
    case types.REQUEST_OPORTNITY_WALL_COMPLETE:
      return {...state, isLoading: false, properties: action.payload,};
    case types.REQUEST_OPORTNITY_WALL_FAILED:
      return {...state, isLoading: false, error: true};
    default:
      return state;
  }
}

export default propertyReducer;